<?php
/*
 *Author: ATISH SHARMA
 *Purpose: Framework for TechTatva 2014 Website
 */
 
define("event1","constructure");
define("event2","StockMart");
 
 class db						//This class controls all interactions with the database
 {
 	protected $db_hostname="localhost";				//MySQL Hostname	
	protected $db_username="root";					//MySQL Username
	protected $db_password="";					//MySQL Password
	protected $db_name="tt14";					//MySQL Database Name
	//protected $db_hostname="mysql13.000webhost.com";		//MySQL Hostname	
	//protected $db_username="a9993525_mrx1015";			//MySQL Username
	//protected $db_password="";					//MySQL Password
	//protected $db_name="a9993525_tt14";				//MySQL Database Name
	protected $con=null; 				//MySQL Connection Variable
	protected $table_users="users";			//Table for signin/signup
	//Constructure Tables
	protected $constructure_table_players="constructure_players";
	protected $constructure_table_inventory="constructure_inventory";
	protected $constructure_table_transaction="constructure_transaction";
	protected $constructure_table_items="constructure_items";
	protected $constructure_table_sale_items="constructure_sale_items";
	protected $constructure_table_structure="constructure_structure";
	protected $constructure_original_balance=10000;
	protected $constructure_items=array(
					"Drafter","800","50",
					"Drawing Sheet","4","1000",
					"Pins","4","1000",
					"Compass","25","50",
					"Pencil","12","1000"
					);
	protected $constructure_transaction_action_sold="sold";
	protected $constructure_transaction_action_purchased="purchased";
	protected $constructure_crisis_factor;
	protected $constructure_structure=array(1,4,4,1,4);	//This array holds the required quantity of each item
	
	/******************
	*
	*How the constructure_items array works:
	*
	*Element 0: items name
	*Element 1: items original price
	*Element 2: items original quantity
	*	
	*and so on...
	*	
	*******************/
	
	function __construct()
	{
		self::set_timezone();
		//self::error_off();
		self::con();
		self::create_db_structure();
		self::set_constructure_crisis_factor(0.2);
	}
	
	function __destruct()
	{
		self::cl_con();
	}
	
	//Protected functions start from here
	
	protected function con()			//This function establishes the connection
	{
		$this->con=mysqli_connect($this->db_hostname,$this->db_username,$this->db_password) or die();
	}
	
	protected function cl_con()			//This function terminates the connection
	{
		mysqli_close($this->con);
	}
	
	protected function create_db_structure()	//This function creates the whole database with its tables and provides initial population if necessary 
	{
		self::create_db();
		self::create_table_users();
		self::create_table_constructure_inventory();
		self::create_table_constructure_players();
		self::create_table_constructure_transaction();
		self::create_table_constructure_items();
		self::create_table_constructure_sale_items();
		self::create_table_constructure_structure();
		self::populate_table_constructure_items();
		self::create_sm_tables();		//Creates StockMart Tables
	}
	
	protected function create_db()			//Creates Database tt14
	{
		$q="CREATE DATABASE IF NOT EXISTS $this->db_name";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		mysqli_select_db($this->con,$this->db_name) or die(mysqli_error($this->con));
	}
	
	protected function create_table_users()		//Creates the table for signin/signup of users
	{
		$q="CREATE TABLE IF NOT EXISTS $this->table_users
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(300) NOT NULL,
			email VARCHAR(300) NOT NULL,
			password VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			INDEX (name(3)),
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	//Constructure Tables Start
	
	protected function create_table_constructure_players()		//Creates the table for constructure players [CONSTRUCTURE TABLE 1]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_players
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			original_balance VARCHAR(300) NOT NULL,
			current_balance VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_constructure_inventory()	//Creates the table for constructure inventory [CONSTRUCTURE TABLE 2]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_inventory
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			item VARCHAR(300) NOT NULL,
			buying_price VARCHAR(300) NOT NULL,
			buying_date VARCHAR(300) NOT NULL,
			INDEX (email(3)),
			INDEX (item(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_constructure_transaction()	//Creates the table for constructure transaction [CONSTRUCTURE TABLE 3]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_transaction
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			item VARCHAR(300) NOT NULL,
			price VARCHAR(300) NOT NULL,
			action VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			INDEX (email(3)),
			INDEX (action(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_constructure_items()		//Creates the table for constructure items [CONSTRUCTURE TABLE 4]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_items
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(300) NOT NULL,
			original_price VARCHAR(300) NOT NULL,
			prev_price VARCHAR(300) NOT NULL,
			current_price VARCHAR(300) NOT NULL,
			original_qty VARCHAR(300) NOT NULL,
			current_qty VARCHAR(300) NOT NULL,
			latest_sale_date VARCHAR(300),
			INDEX (name(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_constructure_sale_items()	//Creates the table for constructure sale items [CONSTRUCTURE TABLE 5]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_sale_items
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			item VARCHAR(300) NOT NULL,
			selling_price VARCHAR(300) NOT NULL,
			original_price VARCHAR(300) NOT NULL,
			buying_date VARCHAR(300) NOT NULL,
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_constructure_structure()	//Creates the table for people with complete structure [CONSTRUCTURE TABLE 6]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->constructure_table_structure
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			name VARCHAR(300) NOT NULL,
			remaining_balance VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			INDEX (email(3)),
			INDEX (name(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	//Constructure Tables End
	
	protected function populate_table_constructure_items()
	{
		for($i=0;$i<sizeof($this->constructure_items);$i+=3)
		{
			$name=$this->constructure_items[$i];
			$q="SELECT sno FROM $this->constructure_table_items
			WHERE name='$name'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num=mysqli_num_rows($result);
			if(!$num)
			{
				$price=$this->constructure_items[$i+1];
				$qty=$this->constructure_items[$i+2];
				$q="INSERT INTO $this->constructure_table_items (name,original_price,prev_price,current_price,original_qty,current_qty) 
				VALUES(
				'$name',
				'$price',
				'$price',
				'$price',
				'$qty',
				'$qty'
				)";
				mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			}
		}
	}
	
	protected function check_winner($email)
	{
		$q="SELECT name FROM 
		$this->constructure_table_structure WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num>0)return true;
		return false;
	}
	
	protected function insert_winner($email)
	{
		$balance=self::get_raw_constructure_balance($email);
		$date=self::get_date();
		$name=strtolower(self::get_name($email));
		$q="INSERT INTO $this->constructure_table_structure (email,name,remaining_balance,date)
		VALUES ('$email','$name','$balance','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function submit_complete_structure($email)
	{
		if(!self::check_winner($email))
		{
			self::insert_winner($email);
		}
	}
	
	protected function insert_into_constructure_table_inventory($email,$item)
	{
		$price=self::get_constructure_item_price($item);
		$date=self::get_date();
		$q="INSERT INTO $this->constructure_table_inventory (email,item,buying_price,buying_date)
		VALUES ('$email','$item','$price','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function insert_into_constructure_table_sale_items($email,$item,$price,$original_price,$buying_date)
	{
		$q="INSERT INTO $this->constructure_table_sale_items (email,item,selling_price,original_price,buying_date)
		VALUES ('$email','$item','$price','$original_price','$buying_date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function insert_into_constructure_table_transaction($email,$item,$action)
	{
		$price=self::get_constructure_item_price($item);
		$date=self::get_date();
		$q="INSERT INTO $this->constructure_table_transaction (email,item,price,action,date)
		VALUES ('$email','$item','$price','$action','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_constructure_item_table()
	{
		$table="
			<table id='buy_from_market' class='center' style='display:none;border-collapse:collapse;' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td>S.no.</td>
				<td>Item</td>
				<td>Price</td>
				<td>Available Quantity</td>
				<td>Purchase Quantity</td>
				<td>Action</td>
			</tr>
		";
		$count=0;
		for($i=0;$i<sizeof($this->constructure_items);$i+=3)
		{
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$number=($i/3)+1;
			$date=self::get_date();
			$item=$this->constructure_items[$i];
			$name=$item;
			$name=explode(" ",$name);
			$id="";
			for($j=0;$j<sizeof($name);$j++)
			{
				$id.=$name[$j];
			}
			$id=trim($id);	
			$value=self::get_constructure_item_value($item);
			$price=self::fix_currency($value[0]);
			$qty=$value[1];
			$table.="
						<tr class='black-font $class'>
							<td>$number.</td>
							<td>$item</td>
							<td align='right' id='market_price_$id'>Rs. $price</td>
							<td align='center' id='market_qty_$id'>$qty</td>
							<td><input type='text' value='' class='$id' size='8px'/></td>
							<td>
								<input type='checkbox' value='buy' class='$id'/> Buy
								<input type='hidden' class='$id' value='$item'/>
							</td>
						</tr>	
			";
		}
		$table.="
		<tr style='background-color:white;'>
			<td class='black-font' colspan='6' align='right'>
			<input type='button' value='Buy Selected Items' class='market_buy'/>
			</td>
		</tr>
		<tr >
			<td id='market_buy_error' style='background-color:red;display:none;color:white;font-weight:bold' colspan='6' align='center'>
			</td>
		</tr>
		<tr style='background-color:white;'>
			<td class='black-font' colspan='6' align='right'>
			*The given stock information was last updated on <span id='market_update_date'>$date</span>	
			</td>
		</tr>
		</table>";
		return $table;
	}
	
	protected function validate_constructure_transaction($email,$item,$qty)
	{
		$balance=self::get_raw_constructure_balance($email);
		$value=self::get_constructure_item_value($item);
		$price=$value[0];
		$quantity=$value[1];
		if($qty<=$quantity)
		{
			if(($price*$qty)<=$balance)
			{
				return true;
			}
		}
		return false;
	}
	
	protected function decrease_constructure_item_count($item,$qty)
	{
		$q="SELECT current_qty FROM 
		$this->constructure_table_items WHERE name='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$old_qty=$row[0];
		$new_qty=$old_qty-$qty;
		$q="UPDATE $this->constructure_table_items 
		SET current_qty='$new_qty' WHERE name='$item'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function get_constructure_item_price($item)
	{
		$q="SELECT current_price FROM 
		$this->constructure_table_items WHERE name='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		return $row[0];
	}
	
	protected function constructure_buy_from_market($email,$item,$qty)
	{
		if(self::validate_constructure_transaction($email,$item,$qty))
		{
			self::decrease_constructure_item_count($item,$qty);
			for($i=0;$i<$qty;$i++)
			{
				self::change_balance($email,$item);
				self::insert_into_constructure_table_inventory($email,$item);
				self::insert_into_constructure_table_transaction($email,$item,$this->constructure_transaction_action_purchased);
			}
			self::set_constructure_item_latest_sale_date($item);
			self::reset_constructure_item_price($item);
			return true;
		}
		return false;
	}
	
	protected function change_balance($email,$item)
	{
		$val=self::get_constructure_item_value($item);
		$price=$val[0];
		$balance=self::get_raw_constructure_balance($email);
		$new_balance=$balance-$price;
		$q="UPDATE $this->constructure_table_players
		SET current_balance='$new_balance' WHERE email='$email'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function add_balance($email,$price)
	{
		$balance=self::get_raw_constructure_balance($email);
		$new_balance=$balance+$price;
		$q="UPDATE $this->constructure_table_players
		SET current_balance='$new_balance' WHERE email='$email'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function minus_balance($email,$price)
	{
		$balance=self::get_raw_constructure_balance($email);
		$new_balance=$balance-$price;
		$q="UPDATE $this->constructure_table_players
		SET current_balance='$new_balance' WHERE email='$email'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function reset_constructure_item_price($item)
	{
		$q="SELECT * FROM 
		$this->constructure_table_items WHERE name='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$name=$row[1];
		$original_price=$row[2];
		$prev_price=$row[3];
		$current_price=$row[4];
		$original_qty=$row[5];
		$current_qty=$row[6];
		$latest_sale_date=$row[7]; 
		$percentage=(float)($original_qty-$current_qty)/(float)$original_qty;
		$factor=5;
		if($percentage>=0.95)$factor=80;
		else if($percentage>=0.9) $factor=75;
		else if($percentage>=0.75) $factor=60;
		else if($percentage>=0.6) $factor=45;
		else if($percentage>=0.5) $factor=40;
		else if($percentage>=0.4) $factor=20;
		else if($percentage>=0.2)$factor=12;
		if($current_qty==1)  $factor=120;
		$factor*=$this->constructure_crisis_factor;
		$new_price=($original_price*(($percentage+1)*$factor/100))+$current_price;
		$q="UPDATE $this->constructure_table_items 
		SET current_price='$new_price',prev_price='$current_price' WHERE name='$item'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function remove_from_constructure_inventory($email,$item)
	{	
		$q="DELETE*FROM
		$this->constructure_table_inventory WHERE email='$email AND item='$item'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function get_inventory_item_value($email,$item)
	{
		$q="SELECT buying_price FROM
		$this->constructure_table_inventory WHERE email='$email' AND item='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$n=mysqli_num_rows($result);
		$action=$this->constructure_transaction_action_purchased;
		$q="SELECT price FROM 
		$this->constructure_table_transaction WHERE email='$email' AND item='$item' AND action='$action'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		$total=0;
		if($num)
		{
			for($i=0;$i<$num;$i++)
			{
				$row=mysqli_fetch_row($result);
				$total+=$row[0];
			}
			$avg=$total/$num;
			$val=array($n,$avg);
			return $val;
		}
		return array(0,"-");
	}
	
	protected function set_constructure_crisis_factor($f)
	{
		$this->constructure_crisis_factor=$f;
	}
	
	protected function set_constructure_item_latest_sale_date($item)
	{
		$date=self::get_date();
		$q="UPDATE $this->constructure_table_items 
		SET latest_sale_date='$date' WHERE name='$item'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_user($name,$email,$password)
	{
		$name=strtolower($name);
		$email=strtolower($email);
		$date=self::get_date();
		$q="INSERT INTO $this->table_users (name,email,password,date)
		VALUES('$name','$email','$password','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}

	protected function verify_email_password_combo($email,$password)
	{
		$email=strtolower($email);
		$q="SELECT password FROM $this->table_users WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num)
		{
			$row=mysqli_fetch_row($result);
			if($password==$row[0])
			{
				return true;
			}
		}
		return false;
	}
	
	protected function check_uniqueness_of_email($email)
	{
		$email=strtolower($email);
		$q="SELECT email FROM $this->table_users WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if(!$num)return true;
		return false;
	}
	
	protected function delete_from_constructure_inventory($email,$item)
	{
		$q="SELECT sno FROM 
		$this->constructure_table_inventory WHERE email='$email' AND item='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$sno=$row[0];
		$q="DELETE FROM 
		$this->constructure_table_inventory WHERE email='$email' AND item='$item' AND sno='$sno'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function take_from_constructure_inventory($email,$item)
	{
		$q="SELECT buying_price,buying_date FROM
		$this->constructure_table_inventory WHERE email='$email' AND item='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		self::delete_from_constructure_inventory($email,$item);
		return $row;
	}
	
	protected function check_constructure_player_existence($email)	//Checks if the users constructure account has already been initiated
	{
		if(trim($email)=="") return true;
		$q="SELECT email FROM $this->constructure_table_players 
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num) return true;
		return false;
	}
	
	protected function sanitize_data($data)
	{
		$data=strip_tags($data);
		$data=htmlentities($data);
		if (get_magic_quotes_gpc())$data=stripslashes($data);
		$data=mysqli_real_escape_string($this->con,$data);
		return $data;
	}
	
	protected function set_timezone()
	{
		 date_default_timezone_set("Asia/Kolkata");
	}
	
	protected function error_off()
	{
		error_reporting(0);
	}
	
	//Public (and Static) functions start from here
	
	public function sign_in($email,$password)
	{
		$email=strtolower(self::sanitize_data($email));
		$password=self::sanitize_data($password);
		return self::verify_email_password_combo($email,$password);
	}
	
	public function sign_up($name,$email,$password)
	{
		$name=strtolower(self::sanitize_data($name));
		$email=strtolower(self::sanitize_data($email));
		$password=self::sanitize_data($password);
		if(self::check_uniqueness_of_email($email))
		{
			self::create_user($name,$email,$password);
			return true;
		}
		return false;
	}
	
	public function create_new_constructure_player($email)		//Creates a new constructure player by activating the users constructure account
	{
		$email=strtolower(self::sanitize_data($email));
		if(!self::check_constructure_player_existence($email))
		{
			$balance=$this->constructure_original_balance;
			$date=self::get_date();
			$q="INSERT INTO $this->constructure_table_players (email,original_balance,current_balance,date)
			VALUES('$email','$balance','$balance','$date')";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
	}
	
	public function get_constructure_structure($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$table="<table style='border-collapse:collapse;' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='5' align='center'><u>My Structure</u></td>
			</tr>
			<tr align='center' class='red-font' style='background-color:white;'>
				<td align='left'>S.no.</td>
				<td>Item Name</td>
				<td>Required Quantity</td>
				<td>Quantity in Inventory</td>
				<td>Completion Status</td>
			</tr>";
		$count=0;
		for($i=0,$j=0;$i<sizeof($this->constructure_items);$i+=3,$j++)
		{
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$item=$this->constructure_items[$i];
			$val=self::get_inventory_item_value($email,$item);
			$qty=$val[0];
			$required=$this->constructure_structure[$j];
			$completion="Incomplete";
			if($required<=$qty)$completion="<font color='green'>COMPLETE</font>";
			$table.="<tr class='black-font $class' align='center'>
				<td>$count.</td>
				<td align='left'>$item</td>
				<td>$required</td>
				<td>$qty</td>
				<td>$completion</td>
			</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function get_constructure_transaction($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$table="<table style='border-collapse:collapse;' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='5' align='center'><u>My Transactions</u></td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>S.no.</td>
				<td>Item</td>
				<td>Price</td>
				<td>Action</td>
				<td align='center'>Date</td>
			</tr>";
		$count=0;
		$q="SELECT item,price,action,date FROM
		$this->constructure_table_transaction WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		for($i=0;$i<$num;$i++)
		{
			mysqli_data_seek($result,$num-($i+1));
			$row=mysqli_fetch_row($result);
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$item=$row[0];
			$price=self::fix_currency($row[1]);
			$action=self::fix_name($row[2]);
			$date=$row[3];
			$table.="<tr class='black-font $class'>
				<td>$count.</td>
				<td>$item</td>
				<td align='right'>Rs. $price</td>
				<td align='center'>$action</td>
				<td align='center'>$date</td>
			</tr>";
		}
		$table.="</table>";
		if($num==0)
		return "<div class='white'>You are yet to make your first transaction.</div>";
		return $table;
	}
	
	public function get_constructure_inventory($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$table="<table style='border-collapse:collapse;' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='5' align='center'><u>My Inventory</u></td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>S.no.</td>
				<td>Item</td>
				<td>Market Price</td>
				<td>Average Buying Price</td>
				<td>Purchased Quantity</td>
			</tr>";
		$count=0;
		for($i=0;$i<sizeof($this->constructure_items);$i+=3)
		{
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$item=$this->constructure_items[$i];
			$val=self::get_inventory_item_value($email,$item);
			$qty=$val[0];
			$avg=$val[1];
			$align='center';
			if($avg!="-")
			{
				$avg=self::fix_currency($avg);
				$avg="Rs. ".$avg;
				$align='right';
			}
			$val=self::get_constructure_item_value($item);
			$price=self::fix_currency($val[0]);
			$table.="<tr class='black-font $class'>
				<td>$count.</td>
				<td>$item</td>
				<td align='right'>Rs. $price</td>
				<td align='$align'>$avg</td>
				<td align='center'>$qty</td>
			</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function get_sell_table($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$table="<table align='center' style='border-collapse:collapse;margin-top:75px' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='6' align='center'><u>Put Your Stocks on Sale</u></td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>S.no.</td>
				<td>Item</td>
				<td>Market Price</td>
				<td>Average Buying Price</td>
				<td>Quantity in Inventory</td>
				<td align='center'>Action</td>
			</tr>";
		$count=0;
		for($i=0;$i<sizeof($this->constructure_items);$i+=3)
		{
			$c=$count;
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$item=$this->constructure_items[$i];
			$val=self::get_inventory_item_value($email,$item);
			$qty=$val[0];
			$avg=$val[1];
			$align='center';
			if($avg!="-")
			{
				$avg=self::fix_currency($avg);
				$avg="Rs. ".$avg;
				$align='right';
			}
			$val=self::get_constructure_item_value($item);
			$price=self::fix_currency($val[0]);
			$dis="";
			if($qty<1 || $avg=="-")$dis="disabled";
			$table.="<tr class='black-font $class'>
				<td>$count.</td>
				<td>$item</td>
				<td align='right'>Rs. $price</td>
				<td align='$align'>$avg</td>
				<td align='center'>$qty</td>
				<td align='right'><input type='button' value='Put this Item on Sale' name='sell' $dis class='chk' id='$c'/>
				<form action='sale.php' method='POST'>
				<table id='sell-this$c' style='background-color:white;border-radius:3px;padding:3px;' class='a_table'>
				<tr align='right'>
				<td>
				Quantiy to Sell : </td><td><input type='number' min='0' max='$qty' $dis name='qty' class='qty$c' step='1'/>
				</td>
				<tr align='right'>
				</tr>
				<td>
				Unit Selling Price <sub>[Rs]</sub> : </td><td><input type='number' min='0' $dis name='price' class='price$c' step='0.01'/>
				</td>
				<tr>
				<td colspan='2' align='right'><input type='submit' value='Place this on Sale' class='sell' name='$c' ></td>
				</tr>
				<input type='hidden' value='$item' name='item'/>
				</tr>
				</table></form></td>
				</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function restore_stock($email,$sno)
	{
		$email=strtolower(self::sanitize_data($email));
		$sno=self::sanitize_data($sno);
		$q="SELECT original_price,buying_date,item FROM
		$this->constructure_table_sale_items WHERE sno='$sno' AND email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$price=$row[0];
		$date=$row[1];
		$item=$row[2];
		$q="INSERT INTO $this->constructure_table_inventory (email,item,buying_price,buying_date)
		VALUES ('$email','$item','$price','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$q="DELETE FROM
		$this->constructure_table_sale_items WHERE sno='$sno' AND email='$email'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	public function get_table_restore_sale_item($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$table="<form action='restore.php' method='POST'>
			<table style='border-collapse:collapse;' class='center' cellpadding='9px' width='65%'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='4' align='center'><u>My Sale</u></td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='4' align='right'><input type='button' value='Select All' id='all'/><input type='button' value='Deselect All' id='none'/></td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>S.no.</td>
				<td>Item</td>
				<td>Sale Price</td>
				<td>Action</td>
			</tr>";
		$count=0;
		$q="SELECT sno,item,selling_price FROM
		$this->constructure_table_sale_items WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		for($i=0;$i<$num;$i++)
		{
			mysqli_data_seek($result,$num-($i+1));
			$row=mysqli_fetch_row($result);
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$sno=$row[0];
			$item=$row[1];
			$price=self::fix_currency($row[2]);
			$table.="<tr class='black-font $class'>
				<td>$count.</td>
				<td>$item</td>
				<td>Rs. $price</td>
				<td><input type='checkbox' value='$sno' name='$i'>Restore</td>
			</tr>";
		}
		$table.="<tr>
			<td colspan='4' align='right' class='red-font' style='background-color:white;'>
				<input type='submit' value='Restore Selected Items'/>
				<input type='hidden' value='$num' name='num'/>
			</td>
			</tr>
			</table>
			</form>";
		if($num==0)return '<div class="white center">None of your stocks are on sale.</div>';
		return $table;
	}
	
	public function sell_others_stock($email,$price,$qty,$item)
	{
		$email=strtolower(self::sanitize_data($email));
		$price=self::sanitize_data($price);
		$qty=self::sanitize_data($qty);
		$item=self::sanitize_data($item);
		$balance=self::get_raw_constructure_balance($email);
		if($balance<($qty*$price))return array(0,"0.00");
		$q="SELECT sno,selling_price,item,email FROM 
		$this->constructure_table_sale_items WHERE email <> '$email' AND item='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		$prc=array();
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);	
			$sno=$row[0];
			$selling_price=$row[1];
			if($selling_price<=$price)$prc[]=$sno;
		}
		$return_qty=sizeof($prc);
		if($return_qty>$qty)$return_qty=$qty;
		$total=0;
		if($return_qty==0)return array(0,"0.00");
		for($i=0;$i<$return_qty;$i++)
		{
			$s=$prc[$i];
			$q="SELECT sno,selling_price,item,email FROM 
			$this->constructure_table_sale_items WHERE sno='$s' AND email <> '$email' AND item='$item'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$row=mysqli_fetch_row($result);
			$sno=$row[0];
			$price=$row[1];
			$total+=$price;
			$item=$row[2];
			$seller_email=$row[3];
			$date=self::get_date();
			$q="INSERT INTO $this->constructure_table_inventory (email,item,buying_price,buying_date)
			VALUES ('$email','$item','$price','$date')";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$q="DELETE FROM 
			$this->constructure_table_sale_items WHERE sno='$sno'";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$action=$this->constructure_transaction_action_sold;
			$q="INSERT INTO $this->constructure_table_transaction (email,item,price,action,date)
			VALUES ('$seller_email','$item','$price','$action','$date')";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			self::add_balance($seller_email,$price);
			$action=$this->constructure_transaction_action_purchased;
			$q="INSERT INTO $this->constructure_table_transaction (email,item,price,action,date)
			VALUES ('$email','$item','$price','$action','$date')";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			self::minus_balance($email,$price);
		}
		return array($return_qty,self::fix_currency($total));
	}
	
	public function get_completion_date($email)
	{
		$q="SELECT date FROM 
		$this->constructure_table_structure WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		return $row[0];
	}
	
	public function get_completion_balance($email)
	{
		$q="SELECT remaining_balance FROM 
		$this->constructure_table_structure WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$balance=self::fix_currency($row[0]);
		return $balance;
	}
	
	public function check_structure($email)
	{
		$email=strtolower(self::sanitize_data($email));
		for($i=0,$j=0;$i<sizeof($this->constructure_items);$i+=3,$j++)
		{
			$item=$this->constructure_items[$i];
			$val=self::get_inventory_item_value($email,$item);
			$qty=$val[0];
			$required=$this->constructure_structure[$j];
			if($required>$qty) return false;
		}
		self::submit_complete_structure($email);
		return true;
	}
	
	public function get_others_stocks()
	{
		$select="<select class='hand-cursor' name='item'>";
		for($i=0;$i<sizeof($this->constructure_items);$i+=3)
		{
			$item=$this->constructure_items[$i];
			$val=self::get_constructure_item_value($item);
			$price=$val[0];
			$price=self::fix_currency($price);
			$select.="<option value='$item'>$item - Market Price: Rs. $price</option>";
		}
		$select.="</select>";
		$table="<form action='buy_others.php' method='POST' class='white'>
			<table style='border:10px solid white;background-color:green;margin-top:120px;padding:21px;border-radius:12px;' align='center'>
			<tr>
				<td align='center' colspan='2'>
					<u>Buy Stocks from Other Shareholders</u>
					<hr width='100%'/>
				</td>
			</tr>
			<tr>
			<td align='right'>
				Select Item : 
			</td>
			<td>
				$select
			</td>
			</tr>
			<tr>
			<td align='right'>
				Select maximum quanity you want to purchase : 
			</td>
			<td>
				<input type='number' min='1' name='qty' id='qty' step='1'/>
			</td>
			</tr>
			<tr>
			<td align='right'>
				Select maximum price you are willing to pay per unit <sub>[Rs]</sub> : 
			</td>
			<td>
				<input type='number' min='1' name='price' step='0.01' id='price'/>
			</td>
			</tr>
			<tr>
			<td colspan='2' align='right'>
				<input type='submit' value='Send Request'/>
			</td>
			</tr>
			</table>
			</form>";
		return $table;
	}
	
	public function get_constructure_winners()
	{
		$table="<table style='border-collapse:collapse;' align='center' cellpadding='9px' width='65%'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='4' align='center'><u>Leaderboard</u><br/>
				<sub>[... people mentioned below have completed their structure ...]</sub>
				</td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>Rank</td>
				<td>Name</td>
				<td>Remaining Balance</td>
				<td>Date of Completion</td>
			</tr>";
		$count=0;
		$q="SELECT name,remaining_balance,date FROM
		$this->constructure_table_structure
		ORDER BY remaining_balance DESC";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return false;
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$name=self::fix_name($row[0]);
			$price=self::fix_currency($row[1]);
			$date=$row[2];
			$table.="<tr class='black-font $class'>
				<td># $count</td>
				<td>$name</td>
				<td>Rs. $price</td>
				<td>$date</td>
			</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function get_constructure_winners_main()
	{
		$table="<table style='border-collapse:collapse;' cellpadding='9px'>
			<tr class='red-font' style='background-color:white;'>
				<td colspan='4' align='center'><u>Leaderboard</u><br/>
				<sub>[... people mentioned below have completed their structure ...]</sub>
				</td>
			</tr>
			<tr class='red-font' style='background-color:white;'>
				<td>Rank</td>
				<td>Name</td>
				<td>Remaining Balance</td>
				<td>Date of Completion</td>
			</tr>";
		$count=0;
		$q="SELECT name,remaining_balance,date FROM
		$this->constructure_table_structure
		ORDER BY remaining_balance DESC";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return false;
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$count++;
			if($count%2==0)$class='orange';
			else $class='yellow';
			$name=self::fix_name($row[0]);
			$price=self::fix_currency($row[1]);
			$date=$row[2];
			$table.="<tr class='black-font $class'>
				<td># $count</td>
				<td>$name</td>
				<td>Rs. $price</td>
				<td>$date</td>
			</tr>";
		}
		$table.="</table>";
		return $table;
	}
	
	public function sell_constructure_item($email,$item,$price,$qty)
	{
		$email=strtolower(self::sanitize_data($email));
		$item=self::sanitize_data($item);
		$qty=self::sanitize_data($qty);
		$price=self::sanitize_data($price);
		for($i=0;$i<$qty;$i++)
		{
			$row=self::take_from_constructure_inventory($email,$item);
			$original_price=$row[0];
			$buying_date=$row[1];
			self::insert_into_constructure_table_sale_items($email,$item,$price,$original_price,$buying_date);
		}
	}
	
	public function get_constructure_item_value($item)
	{
		$item=self::sanitize_data($item);
		$q="SELECT current_price,current_qty FROM 
		$this->constructure_table_items WHERE name='$item'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		return $row;
	}
	
	public function get_raw_constructure_balance($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT current_balance FROM $this->constructure_table_players
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		return $row[0];
	}
	
	public function buy($email,$item,$qty)
	{
		$email=strtolower(self::sanitize_data($email));
		$item=self::sanitize_data($item);
		$qty=self::sanitize_data($qty);
		return self::constructure_buy_from_market($email,$item,$qty);
	}
	
	public function get_constructure_item_table()
	{
		$table=self::create_constructure_item_table();
		return $table;
	}
	
	public function get_constructure_balance($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$balance=self::get_raw_constructure_balance($email);
		return self::fix_currency($balance);
	}
	
	public function get_name($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT name FROM $this->table_users WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$name=self::fix_name($row[0]);
		return $name;
	}
	
	public static function logout()
	{
		session_start();
		session_destroy();
		header('Location:../../../index.php');
	}
	
	public static function fix_currency($currency)
	{
		setlocale(LC_MONETARY,'en_IN');
		$currency=money_format('%!i',$currency);
		return $currency;
	}
	
	public static function fix_name($name)
	{
		$n=explode(' ',$name);
		$name="";
		for($i=0;$i<sizeof($n);$i++)
		{
			$name.=ucfirst(strtolower($n[$i]));	
			if($i<sizeof($n)-1)
			{
				$name.=" ";
			}
		}
		return $name;
	}
	
	public static function get_date()
	{
		return date('l jS \of F Y h:i:s A');	
	}
	
	//All methods here onwards belong to StockMart
	
	protected $sm_table_players="sm_players";
	protected $sm_table_stocks="sm_stocks";
	protected $sm_table_transactions="sm_transactions";
	protected $sm_table_orders="sm_orders";
	protected $sm_stocks=array("Guitar",200,5,
				"Ball",150,30,
				"Soap",200,1000,
				"Random Stuff",1200,1000);
	//The sm_stocks array works just like the constructure_items array ('stock name','original pice','original quantity')
	protected $sm_original_balance=10000;
	protected $sm_owner_market="market";
	protected $sm_transaction_sold="sold";
	protected $sm_transaction_purchased="purchased";
	protected $sm_state_sale="on sale";
	protected $sm_state_inventory="in inventory";
	protected $sm_state_active="active";
	protected $sm_state_inactive="inactive";
	
	protected function create_table_sm_players()		//Creates the table for StockMart players [STOCKMART TABLE 1]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->sm_table_players
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			current_balance VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			diff VARCHAR(300) NOT NULL,
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_sm_stocks()		//Creates the table for StockMart stocks [STOCKMART TABLE 2]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->sm_table_stocks
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			stock VARCHAR(300) NOT NULL,
			owner VARCHAR(300) NOT NULL,
			state VARCHAR(300) NOT NULL,
			price VARCHAR(300) NOT NULL,
			prev_price VARCHAR(300) NOT NULL,
			INDEX (stock(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_sm_transactions()	//Creates the table for StockMart transactions [STOCKMART TABLE 3]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->sm_table_transactions
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			stock_id VARCHAR(300) NOT NULL,
			price VARCHAR(300) NOT NULL,
			action VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function create_table_sm_orders()	//Creates the table for StockMart Orders [STOCKMART TABLE 4]
	{
		$q="CREATE TABLE IF NOT EXISTS $this->sm_table_orders
		(
			sno BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(300) NOT NULL,
			stock VARCHAR(300) NOT NULL,
			quantity VARCHAR(300) NOT NULL,
			price VARCHAR(300) NOT NULL,
			date VARCHAR(300) NOT NULL,
			time VARCHAR(300) NOT NULL,
			state VARCHAR(300) NOT NULL,
			INDEX (email(3))
		)
		ENGINE MyISAM";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function populate_sm_stocks()		
	{
		for($i=0;$i<sizeof($this->sm_stocks);$i+=3)
		{
			$stock=$this->sm_stocks[$i];
			$q="SELECT sno FROM $this->sm_table_stocks
			WHERE stock='$stock'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num=mysqli_num_rows($result);
			if(!$num)
			{
				$price=$this->sm_stocks[$i+1];
				$num=$this->sm_stocks[$i+2];
				$owner=$this->sm_owner_market;
				$state=$this->sm_state_sale;
				for($j=0;$j<$num;$j++)
				{
					$q="INSERT INTO $this->sm_table_stocks (stock,owner,state,price,prev_price)
					VALUES ('$stock','$owner','$state','$price','$price')";
					mysqli_query($this->con,$q) or die(mysqli_error($this->con));
				}
			}
		}
	}
	
	protected function create_sm_tables()
	{
		self::create_table_sm_players();
		self::create_table_sm_stocks();
		self::create_table_sm_transactions();
		self::create_table_sm_orders();
		self::populate_sm_stocks();
	}
	
	protected function insert_into_table_sm_orders($email,$stock,$qty,$price)
	{
		$date=self::get_date();
		$time=time();
		$state=$this->sm_state_active;
		$q="INSERT INTO $this->sm_table_orders (email,stock,quantity,price,date,time,state)
		VALUES('$email','$stock','$qty','$price','$date','$time','$state')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	public function deactivate_from_table_sm_orders($sno)
	{
		$state=$this->sm_state_inactive;
		$q="UPDATE $this->sm_table_orders
		SET state='$state' 
		WHERE sno='$sno'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function insert_into_table_sm_transactions($email,$stock_id,$price,$action)
	{
		$date=self::get_date();
		$q="INSERT INTO $this->sm_table_transactions (email,stock_id,price,action,date)
		VALUES('$email','$stock_id','$price','$action','$date')";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	protected function validate_sm_transaction($money,$email)
	{
		$balance=self::get_sm_balance($email);
		if($money>$balance)return false;
		return true;
	}
	
	protected function deactivate_expired_sm_orders()
	{
		self::calc_sm_diff();
		$q="SELECT time,sno,quantity FROM $this->sm_table_orders";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		$factor=60*60*3;
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$time=$row[0];
			$sno=$row[1];
			$qty=$row[2];
			$now=time();
			$diff=$now-$time;
			if($diff>=$factor || $qty<1)
			{
				$state=$this->sm_state_inactive;
				$q="UPDATE $this->sm_table_orders
				SET state='$state'
				WHERE sno='$sno'";
				mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			}
		}
	}
	
	protected function get_sm_stocks_avg_price()
	{
		$total=array();
		for($i=0,$k=0;$i<sizeof($this->sm_stocks);$i+=3,$k+=2)
		{
			$total[$k]=0;
			$stock=$this->sm_stocks[$i];
			$q="SELECT price FROM $this->sm_table_stocks
			WHERE stock='$stock'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num=mysqli_num_rows($result);
			for($j=0;$j<$num;$j++)
			{
				$row=mysqli_fetch_row($result);
				$price=$row[0];
				$total[$k]+=$price;
			}
			$total[$k]/=$num;
			$q="SELECT prev_price FROM $this->sm_table_stocks
			WHERE stock='$stock'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$row=mysqli_fetch_row($result);
			$prev_price=$row[0];
			$diff=($total[$k]-$prev_price)*100/$prev_price;
			$total[$k+1]=round($diff,2);
			$q="UPDATE $this->sm_table_stocks
			SET prev_price='$total[$k]'
			WHERE stock='$stock'";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
		return $total;
	}
	
	public function remove_sm_stocks_from_sale($sno)
	{
		$state=$this->sm_state_inventory;
		$sno=strtolower(self::sanitize_data($sno));
		$q="UPDATE $this->sm_table_stocks
		SET state='$state'
		WHERE sno='$sno'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
	}
	
	public function put_sm_stocks_on_sale($stock,$email,$qty,$price)
	{
		self::deactivate_expired_sm_orders();
		$stock=self::sanitize_data($stock);
		$email=strtolower(self::sanitize_data($email));
		$qty=strtolower(self::sanitize_data($qty));
		$price=strtolower(self::sanitize_data($price));
		if($price<0)return false;
		if($qty<1)return false;
		if(!is_numeric($price))return false;
		if(!is_numeric($qty))return false;
		$qty=round($qty);
		$price=round($price,2);
		$state=$this->sm_state_inventory;
		$q="SELECT sno FROM $this->sm_table_stocks
		WHERE stock='$stock' AND owner='$email' AND state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<$qty)return false;
		$sno_array=array();
		for($i=0;$i<$qty;$i++)
		{
			$row=mysqli_fetch_row($result);
			$sno_array[]=$row[0];
		}
		for($i=0;$i<$qty;$i++)
		{
			$sno=$sno_array[$i];
			$state=$this->sm_state_sale;
			$q="UPDATE $this->sm_table_stocks
			SET state='$state',price='$price'
			WHERE sno='$sno'";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
		self::check_sm_orders();
		return true;
	}
	
	protected function get_sm_diff($email,$id)
	{
		$action=$this->sm_transaction_sold;
		$q="SELECT price
		FROM $this->sm_table_transactions
		WHERE email='$email' AND action='$action' AND stock_id='$id'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num_sold=mysqli_num_rows($result);
		$total_sold=0;
		if($num_sold<1)return 0;
		for($i=0;$i<$num_sold;$i++)
		{
			$row=mysqli_fetch_row($result);
			$total_sold+=$row[$i];
		}
		$action=$this->sm_transaction_purchased;
		$q="SELECT price
		FROM $this->sm_table_transactions
		WHERE email='$email' AND action='$action' AND stock_id='$id'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$total_purchased=0;
		for($i=0;$i<$num_sold;$i++)
		{
			$row=mysqli_fetch_row($result);
			$total_purchased+=$row[$i];
		}
		$total=(($total_sold-$total_purchased)*100)/$total_purchased;
		return $total;
	}
	
	protected function calc_sm_diff()
	{
		$q="SELECT email FROM $this->sm_table_players";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		for($i=0;$i<$num;$i++)
		{
			$total=0;
			$count=0;
			$action=$this->sm_transaction_purchased;
			$row=mysqli_fetch_row($result);
			$email=$row[0];
			$q="SELECT stock_id
			FROM $this->sm_table_transactions
			WHERE email='$email' AND action='$action'";
			$result2=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num2=mysqli_num_rows($result2);
			for($j=0;$j<$num2;$j++)
			{
				$row2=mysqli_fetch_row($result2);
				$stock_id=$row2[0];
				$d=self::get_sm_diff($email,$stock_id);
				if($d!=0)
				{
					$total+=$d;
					$count++;
				}
			}
			if($count!=0)$total/=$count;
			$q="UPDATE $this->sm_table_players
			SET diff='$total'
			WHERE email='$email'";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
	}
	
	public function get_sm_top_gainers_table()
	{
		$rows="";
		$q="SELECT email,diff 
		FROM $this->sm_table_players
		ORDER BY diff DESC";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		$limit=5;//limit
		$count=0;
		if($num<1)return "";
		if($num<$limit)$limit=$num;
		for($i=0;$i<$limit;$i++)
		{
			$count++;
			$row=mysqli_fetch_row($result);
			$email=$row[0];
			$diff=round($row[1],2);
			$name=self::get_name($email);
			$rows.="<tr>
			<td># $count</td>
			<td>$name</td>
			<td align='right'>$diff %</td>
			</tr>";
		}
		$table=<<<_END
		<table class="table table-striped">
			<h3>
				Top Gainers
			</h3>
			<tr style="font-weight:bold;" class="well info">
				<td align='left'>
					Rank
				</td>
				<td align='left'>
					Name
				</td>
				<td align='center'>
					Net Profit
				</td>
			</tr>
			$rows
		</table>
_END;
	return $table;
	}
	
	public function create_sm_player($email)
	{
		$email=strtolower(self::sanitize_data($email));
		if(trim($email)=="")return false;
		$q="SELECT sno FROM $this->sm_table_players
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if(!$num)
		{
			$diff=0;
			$current_balance=$this->sm_original_balance;
			$date=self::get_date();
			$q="INSERT INTO $this->sm_table_players 
			(email,current_balance,date,diff)
			VALUES ('$email','$current_balance','$date','$diff')";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
	}
	
	public function get_actual_sm_balance($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT current_balance 
		FROM $this->sm_table_players
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$balance=$row[0];
		return $balance;
	}
	
	public function get_sm_net_profit($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT diff FROM $this->sm_table_players
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		return $row[0];
	}
	
	public function get_sm_balance($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT current_balance 
		FROM $this->sm_table_players
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$balance=$row[0];
		$state=$this->sm_state_active;
		$q="SELECT quantity,price 
		FROM $this->sm_table_orders
		WHERE email='$email' AND state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		$total=0;
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$money=$row[0]*$row[1];
			$total+=$money;
		}
		$balance-=$total;
		return $balance;
	}
	
	public function get_sm_currency($email)
	{
		$email=strtolower(self::sanitize_data($email));
		return self::fix_currency(self::get_sm_balance($email));
	}
	
	public function sm_place_order($email,$stock,$qty,$price)
	{
		$email=strtolower(self::sanitize_data($email));
		$stock=self::sanitize_data($stock);
		$qty=round(self::sanitize_data($qty));
		$price=round(self::sanitize_data($price),2);
		$money=$qty*$price;
		self::deactivate_expired_sm_orders();
		if(!self::validate_sm_transaction($money,$email)) return false;
		self::insert_into_table_sm_orders($email,$stock,$qty,$price);
		self::check_sm_orders();
		return true;
	}
	
	protected function check_sm_orders()
	{
		$state=$this->sm_state_active;
		$q="SELECT sno,email,stock,quantity,price
		FROM $this->sm_table_orders
		WHERE state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$sno_order=$row[0];
			$email=$row[1];
			$stock_order=$row[2];
			$quantity=$row[3];
			$price_order=$row[4];
			$state=$this->sm_state_sale;
			$q="SELECT sno,owner,stock,price
			FROM $this->sm_table_stocks
			WHERE state='$state' AND owner<>'$email' AND stock='$stock_order'";
			$result2=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num2=mysqli_num_rows($result2);
			$n=$quantity;
			if($num2<$quantity)$n=$num2;
			for($j=0;$j<$n;$j++)
			{
				$row2=mysqli_fetch_row($result2);
				$sno_stock=$row2[0];
				$owner=$row2[1];
				$stock_stock=$row2[2];
				$price_stock=$row2[3];
				if($price_stock<=$price_order)
				{
					self::buy_sm_stock($sno_order,$email,$stock_order,$quantity,$price_order,$sno_stock,$price_stock);
				}
			}
		}
	}
	
	protected function buy_sm_stock($sno,$email,$stock,$qty,$price,$sno2,$cost)
	{
		$q="SELECT quantity 
		FROM $this->sm_table_orders
		WHERE sno='$sno'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$qty=$row[0];
		if($qty<1)return false;
		$state=$this->sm_state_inventory;
		$q="SELECT owner 
		FROM $this->sm_table_stocks
		WHERE sno='$sno2'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$row=mysqli_fetch_row($result);
		$owner=$row[0];
		$q="SELECT current_balance
		FROM $this->sm_table_players
		WHERE email='$owner'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num>0)
		{
			$row=mysqli_fetch_row($result);
			$balance2=$row[0];
			$balance2+=$cost;
			$q="UPDATE $this->sm_table_players
			SET current_balance='$balance2'
			WHERE email='$owner'";
			mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		}
		$q="UPDATE $this->sm_table_stocks
		SET state='$state',owner='$email'
		WHERE sno='$sno2'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$qty--;
		$q="UPDATE $this->sm_table_orders
		SET quantity='$qty'
		WHERE sno='$sno'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$balance=self::get_actual_sm_balance($email);
		$balance=$balance-$cost;
		$action=$this->sm_transaction_purchased;
		self::insert_into_table_sm_transactions($email,$sno2,$cost,$action);
		$action=$this->sm_transaction_sold;
		self::insert_into_table_sm_transactions($owner,$sno2,$cost,$action);
		$q="UPDATE $this->sm_table_players
		SET current_balance='$balance'
		WHERE email='$email'";
		mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		self::deactivate_expired_sm_orders();
	}
	
	protected function get_sm_stock_name($id)
	{
		$q="SELECT stock FROM $this->sm_table_stocks
		WHERE sno='$id'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);	
		if($num<1)return false;
		$row=mysqli_fetch_row($result);
		return $row[0];
	}
	
	protected function get_sm_stock_avg_buying_price($stock,$email)
	{
		$action=$this->sm_transaction_purchased;
		$q="SELECT price,stock_id 
		FROM $this->sm_table_transactions
		WHERE email='$email' AND action='$action'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return false;
		$total=0;
		$count=0;
		for($i=0;$i<$num;$i++)
		{
			$row=mysqli_fetch_row($result);
			$price=$row[0];
			$id=$row[1];
			if($stock==self::get_sm_stock_name($id))
			{
				$count++;
				$total+=$price;
			}
		}
		if($count==0)return "-";
		$total/=$count;
		return $total;
	}
	
	public function get_sm_stock_inventory_qty($email,$stock)
	{
		$email=strtolower(self::sanitize_data($email));
		$stock=self::sanitize_data($stock);
		$state=$this->sm_state_inventory;
		$q="SELECT sno 
		FROM $this->sm_table_stocks
		WHERE owner='$email' AND stock='$stock' AND state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		return $num;
	}
	
	public function get_sm_remove_sale_table($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$state=$this->sm_state_sale;
		$q="SELECT stock,price,sno
		FROM $this->sm_table_stocks 
		WHERE owner='$email' AND state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return "<span class='text-info well col-md-4 col-md-offset-4' >None of your stocks are on sale.<span>";
		$count=0;
		for($i=0;$i<$num;$i++)
		{
			$count++;
			$row=mysqli_fetch_row($result);
			$stock=$row[0];
			$price=self::fix_currency($row[1]);
			$sno=$row[2];
			$rows.="<tr>
			<td>$count.</td>
			<td align='center'>$stock</td>
			<td align='right'>Rs. $price</td>
			<td align='center'><input type='checkbox' value='$sno' name='$count' id='$count'> Remove</td>
			</tr>";
		}
		$table="<form action='remove_sale.php'  method='POST'><table class='table table-striped table-condensed'><h3>My Sale</h3><tr class='well info' style='font-weight:bold;'>
		<input type='hidden' value='$num' name='num_remove_sale'/>
		<td>S.no.</td>
		<td align='center'>Stock</td>
		<td align='center'>Price</td>
		<td align='center'>Action</td>
		</tr>
		$rows
		<tr>
		<td colspan='4' align='right'>
		<input type='button' value='Deselect All' id='deselect_all' class='btn btn-default'>
		<input type='button' value='Select All' id='select_all' class='btn btn-primary'>
		<input type='submit' value='Remove Item(s) from Sale' class='btn btn-danger'/>
		</td>
		</tr>
		</form>
		</table>";
		return $table;
	}
	
	public function get_sm_sell_table($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$avg=self::get_sm_stocks_avg_price();
		$rows="";
		$serial_no=0;
		for($i=0,$k=0;$i<sizeof($this->sm_stocks);$i+=3,$k+=2)
		{
			$serial_no++;
			$mkt_price=self::fix_currency($avg[$k]);
			$stock=$this->sm_stocks[$i];
			$state=$this->sm_state_inventory;
			$q="SELECT sno FROM $this->sm_table_stocks
			WHERE stock='$stock' AND owner='$email' AND state='$state'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num=mysqli_num_rows($result);
			$avg_price=self::get_sm_stock_avg_buying_price($stock,$email);
			if(!$avg_price)return "<span class='text-info well col-md-4 col-md-offset-4' >Your inventory is empty!<span>";
			$disabled="";
			if($avg_price!="-")$avg_price="Average Buying Price Rs. ".self::fix_currency($avg_price);
			else $avg_price="<sub class='text-info'>[You are yet to buy a $stock]</sub>";
			if($num<1)$disabled="disabled";
			$rows.="
			<tr>
				<td>
					$serial_no.
				</td>
				<td>
					$stock
				</td>
				<td align='center' id='prev_qtysell$serial_no'>
					$num
				</td>
				<td align='center'>
					<input type='checkbox' value='$stock' id='$serial_no' $disabled />Sell
				</td>
			</tr>
			<tr style='display:none;' class='i$serial_no'>
				<td align='center' colspan='2'>
					Market Price: Rs. $mkt_price
				</td>
				<td align='center' colspan='2'>
					$avg_price
				</td>
			</tr>
			<tr style='display:none;' class='i$serial_no'>
				<form role='form' class='$serial_no'>
				<td align='left' colspan='2'>
					<input type='hidden' value='$stock' id='stocksell$serial_no' name='sell_stock'/>
					<span id='errorsell$serial_no'></span>
				</td>
				<td align='right' colspan='2'>
					<div class='form-inline' role='form'>
					<div class='form-group' id='div-qty$serial_no'>
					<input type='text' class='$serial_no form-control' placeholder='Quantity to Sell' id='qtysell$serial_no' name='qty$stock'/>
					</div>
					<div class='form-group' id='div-price$serial_no'>
					<div class='input-group'>
					<div class='input-group-addon'>Rs.</div>
					<input class='$serial_no form-control' type='text' placeholder='Unit Sale Price' id='pricesell$serial_no' name='price$stock'/>
					</div>
					</div>
					<input type='submit' value='Sell' id='sell$serial_no' class='btn btn-primary $disabled'/>
					<input type='reset' value='Reset' id='reset$serial_no' class='btn btn-warning $disabled'/>
					</div>
				</td>
				</form>
			</tr>
			";
		}
		$table=<<<_END
		<table class="table">
			<h3>
				Place Stocks on Sale
			</h3>
			<tr style="font-weight:bold;" class="well info">
				<td align='left'>
					S.no.
				</td>
				<td align='left'>
					Stock
				</td>
				<td align='center'>
					Quantity
				</td>
				<td align='center'>
					Action
				</td>
			</tr>
			$rows
		</table>
_END;
	return $table;
	}

	public function get_sm_inventory($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$avg=self::get_sm_stocks_avg_price();
		$rows="";
		$serial_no=0;
		for($i=0,$k=0;$i<sizeof($this->sm_stocks);$i+=3,$k+=2)
		{
			$serial_no++;
			$mkt_price=self::fix_currency($avg[$k]);
			$stock=$this->sm_stocks[$i];
			$state=$this->sm_state_inventory;
			$q="SELECT sno FROM $this->sm_table_stocks
			WHERE stock='$stock' AND owner='$email' AND state='$state'";
			$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
			$num=mysqli_num_rows($result);
			$avg_price=self::get_sm_stock_avg_buying_price($stock,$email);
			if(!$avg_price)return "<span class='text-info well col-md-4 col-md-offset-4' >Your inventory is empty!<span>";
			$align="center";
			if($avg_price!="-"){$avg_price="Rs. ".self::fix_currency($avg_price);$align="right";}
			$rows.="
			<tr>
				<td>
					$serial_no.
				</td>
				<td>
					$stock
				</td>
				<td align='right'>
					Rs. $mkt_price
				</td>
				<td align='$align'>
					$avg_price
				</td>
				<td align='center'>
					$num
				</td>
			</tr>";
		}
		$table=<<<_END
		<table class="table table-striped">
			<h3>
				Inventory
			</h3>
			<tr style="font-weight:bold;" class="well info">
				<td align='left'>
					S.no.
				</td>
				<td align='left'>
					Stock
				</td>
				<td align='center'>
					Market Price
				</td>
				<td align='center'>
					Average Buying Price
				</td>
				<td align='center'>
					Quantity
				</td>
			</tr>
			$rows
		</table>
_END;
	return $table;
	}
	
	public function get_sm_transactions($email)
	{
		$email=strtolower(self::sanitize_data($email));
		$q="SELECT stock_id,price,action,date
		FROM $this->sm_table_transactions 
		WHERE email='$email'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return "<span class='text-info well col-md-4 col-md-offset-4' >You have made no transaction!<span>";
		$count=0;
		for($i=0;$i<$num;$i++)
		{
			$count++;
			mysqli_data_seek($result,$num-1-$i);
			$row=mysqli_fetch_row($result);
			$stock_id=$row[0];
			$price=self::fix_currency($row[1]);
			$action=ucfirst(strtolower($row[2]));
			$date=$row[3];
			$stock=self::get_sm_stock_name($stock_id);
			$rows.="<tr><td>$count.</td><td align='center'>$stock</td><td align='center'>$stock_id</td><td align='center'>$action</td><td align='right'>Rs. $price</td><td align='center'>$date</td></tr>";
		}
		$table="<table class='table table-striped'><h3>Transactions</h3><tr style='font-weight:bold;' class='well info'>
		<td>S.no.</td>
		<td align='center'>Stock</td>
		<td align='center'>Stock ID</td>
		<td align='center'>Action</td>
		<td align='center'>Price</td>
		<td align='center'>Date</td>
		</tr>$rows</table>";
		return $table;
	}
	
	protected function get_remaining_time($time)
	{
		$t=time();
		$t-=$time;
		$t=(60*60*3)-$t;
		$h=(int)($t/(60*60));
		$m=(int)(($t%(60*60))/60);
		$s=(int)((($t%(60*60))%60));
		return "$h:$m:$s";
	}
	
	public function get_sm_active_orders_table($email)
	{
		self::deactivate_expired_sm_orders();
		$email=strtolower(self::sanitize_data($email));
		$state=$this->sm_state_active;
		$q="SELECT stock,quantity,price,date,time,sno
		FROM $this->sm_table_orders 
		WHERE email='$email' AND state='$state'";
		$result=mysqli_query($this->con,$q) or die(mysqli_error($this->con));
		$num=mysqli_num_rows($result);
		if($num<1)return "<span class='text-info well col-md-4 col-md-offset-4' >Either you have placed no order or all your orders have been deactivated!<span>";
		$count=0;
		for($i=0;$i<$num;$i++)
		{
			$count++;
			$row=mysqli_fetch_row($result);
			$stock=$row[0];
			$quantity=$row[1];
			$price=self::fix_currency($row[2]);
			$date=$row[3];
			$time=$row[4];
			$time=self::get_remaining_time($time);
			$sno=$row[5];
			$rows.="<tr>
			<td>$count.</td>
			<td align='center'>$stock</td>
			<td align='center'>$quantity</td>
			<td align='right'>Rs. $price</td>
			<td align='center'>$time</td>
			<td align='center'><input type='checkbox' value='$sno' name='$count' id='$count'> Remove</td>
			</tr>";
		}
		$table="<form action='remove_order.php' method='POST'><table class='table table-striped table-condensed'><h3>Active Orders</h3><tr class='well info' style='font-weight:bold;'>
		<input type='hidden' value='$num' name='num_remove_order'/>
		<td>S.no.</td>
		<td align='center'>Stock</td>
		<td align='center'>Quantity Remaining</td>
		<td align='center'>Price</td>
		<td align='center'>Time to Deactivation</td>
		<td align='center'>Action</td>
		</tr>
		$rows
		<tr>
		<td colspan='7' align='right'><input type='submit' value='Remove Order(s)' class='btn btn-danger'/></td>
		</tr>
		</form>
		</table>";
		return $table;
	}
	
	public function get_sm_place_order_table()
	{
		$rows="";
		$avg=self::get_sm_stocks_avg_price();
		for($i=0,$k=0;$i<sizeof($this->sm_stocks);$i+=3,$k+=2)
		{
			$stock=$this->sm_stocks[$i];
			$price=$avg[$k];
			$diff=$avg[$k+1];
			$cls="warning";
			$sign="=";
			if($diff<0){$cls="danger";$sign="-";}
			else if($diff>0){$cls="success";$sign="+";}
			$price=self::fix_currency($price);
			$serial_no=$k/2+1;
			$rows.="<tr class='$cls'>
			<td>$serial_no. </td>
			<td>$stock</td>
			<td align='right'>Rs. $price</td>
			<td align='right'><kbd>$sign</kbd></td><td align='right'> $diff %</td>
			<td align='center'><input type='checkbox' class='$serial_no' value='$stock' name='chk$serial_no' size='100%'>Order</td>
			<input type='hidden' value='$stock' name='stock$serial_no'/>
			</tr>
			<tr style=display:none;' class='$serial_no'>
			<form role='form' class='$serial_no'>
			<input type='hidden' value='$stock' name='order_stock'>
			<td colspan='2' align='center'>
			<div class='form-inline' role='form'>
			<div class='form-group' id='div-qty$serial_no'>
			<input type='text' class='$serial_no form-control' placeholder='Purchase Quantity' name='qty' id='qty$serial_no'/>
			</div>
			</td>
			<td colspan='2' align='center'>
			<div class='form-group' id='div-price$serial_no'>
			<div class='input-group'>
			<div class='input-group-addon'>Rs.</div>
			<input type='text' class='$serial_no form-control' placeholder='Unit Purchase Price' name='price' id='price$serial_no'/>
			</div>
			</div>
			</td>
			<td colspan='1' align='center'>
				<input type='submit' value='Place Order' class='btn btn-primary' id='submit$serial_no'/>
			</td>
			<td colspan='1' align='center'>
				<input type='reset' class='btn btn-warning' id='reset$serial_no' value='Reset'/>
			</td>
			</div>
			</td>
			</form>
			</tr>
			<tr>
				<td colspan='6' align='center' class='error$serial_no has-error' style='display:none;'>
					
				</td>
			</tr>";
		}
		$table=<<<_END
		<table class="table">
			<h3>
					Place Order
			</h3>
			<tr style="font-weight:bold;" class="well info">
				<td align='left'>
					S.no.
				</td>
				<td align='left'>
					Stock
				</td>
				<td align='center'>
					Market Price
				</td>
				<td align='center' colspan='2'>
					Differential
				</td>
				<td align='center'>
					Action
				</td>
			</tr>
			$rows
		</table>
_END;
	return $table;
	}
	
	public function echo_sm_template($email,$content)
	{
		$email=strtolower(self::sanitize_data($email));
		$name=self::get_name($email);
		$balance=self::get_sm_currency($email);
		$event="StockMart";
		$page = <<<_END
	<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../JavaScript Plugins/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet"> 
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
    <script src="../../JavaScript Plugins/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <title>$event</title>
  </head>
  <body style="padding-top:70px;">
    
    <!-- navbar starts from here -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    	<div class="container">  
    		<div class="navbar-brand">$event</div>
    		<div class="collapse navbar-collapse">
    			<ul class="nav navbar-nav">
    			<li><a href="home.php">Home</a></li>
    			<li class="dropdown">
    				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    					My Stocks
    					<span class="caret"></span>
    				</a>
    				<ul class="dropdown-menu">
    					<li><a href="portfolio.php">Portfolio</a></li>
    					<li><a href="inventory.php">Inventory</a></li>
    					<li><a href="transactions.php">Transactions</a></li>
    				</ul>
    			</li>
    			<li class="dropdown">
    				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    					Stock Market
    					<span class="caret"></span>
    				</a>
    				<ul class="dropdown-menu">
    					<li class="dropdown-header">Buy</li>
    					<li><a href="order.php">Place Order</a></li>
    					<li><a href="active_order.php">View Active Orders</a></li>
    					<li class="divider"></li>
    					<li class="dropdown-header">Sell</li>
    					<li><a href="sell.php">Place my Stocks on Sale</a></li>
    					<li><a href="my_sale.php">View my Stocks on Sale</a></li>
    				</ul>
    			</li>
    			</ul>
    			<ul class="nav navbar-nav navbar-right">
    				<li>
    					<a href="#">$name</a>
    				</li>
    				<li>
    					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
    					<ul class="dropdown-menu">
    					<li><a href="#">Email : $email</a></li>
    					<li><a href="#" id='my_balance'>Balance : Rs. $balance</a></li>
    					<li><a href="logout.php">Logout</a></li>
    					</ul>
    				</li>
    			</ul>
    		</div>
    	</div>
    </div>
    <!-- navbar ends here -->
    
    <!-- jumbotron starts from here -->
    <div class="jumbotron col-md-8 col-md-offset-2">
    	<div class="container-fluid">
    		$content
    	</div>	
</div>	
    <!-- jumbotron ends here -->
    <div id="footer" style="position:absolute;bottom:0px;right:0px;font-weight:bold;font-size:12px;">&copy;TechTatva 2014</div>
  </body>
  <input type='hidden' value='$email' id='email'>
</html>		
_END;
	return $page;
	}
	
 }
?>
