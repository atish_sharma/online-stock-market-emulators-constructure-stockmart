$(document).ready(function(){
	$('#signin').validate({
					rules:{
						email:{
							required:true,
							email:true
						},
						password:{
							required:true
						}
					},
					messages:{
						email:{
							required:"Please enter your email",
							email:"Please enter a valid email"
						},
						password:{
							required:"Please enter your password"
						}
					}
				});
			$('#signin').submit(function(event){
				event.preventDefault();
				var flag=true;
				$('#signin input.error').each(function(){
						flag=false;
				});
				if(flag)
				{
					var query=$('#signin').serialize();
					$.post('signin.php',query,function(data){
						data=$.trim(data);
						if(data=="fail")
						{
							$('#signin_error').text("Wrong email/password combination");
						}
						else if(data=="pass")
						{
							$('#signin_error').text("");
							window.location="home.php";
						}
					});
				}
			});
			$('#signup').validate({
				rules:{
						name:{
							required:true
						},
						email:{
							required:true,
							email:true
						},
						password1:{
							required:true,
							rangelength:[8,16]
						},
						password2:{
							equalTo:'#password'
						}
					},
					messages:{
						name:{
							required:"Please enter your name"
						},
						email:{
							required:"Please enter your email",
							email:"Please enter a valid email"
						},
						password1:{
							required:"Please enter a password",
							rangelength:"Password should be 8 to 16 characters long"
						},
						password2:{
							equalTo:"Passwords do not match"
						}
					}
				});
			$('#signup').submit(function(event){
				event.preventDefault();
				var flag=true;
				$('#signup input.error').each(function(){
						flag=false;
				});
				if(flag)
				{
					var query=$('#signup').serialize();
					$.post('signup.php',query,function(data){
						data=$.trim(data);
						if(data=="fail")
						{
							$('#signup_error').text("This email is already in use :-(");
						}
						else if(data=="pass")
						{
							$('#signup_error').text("");
							window.location="home.php";
						}
						});
				}
			});
});
