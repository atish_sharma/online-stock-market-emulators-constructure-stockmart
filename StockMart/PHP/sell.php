<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$db->create_sm_player($email);
$event=event2;
$content=$db->get_sm_sell_table($email);
echo $db->echo_sm_template($email,$content); 
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type="checkbox"]').click(function(){
				var chk_id=$(this).attr('id');
				if($(this).prop('checked')==true)
				{
					$('.i'+chk_id).fadeIn(200);
				}
				else
				{
					$('.i'+chk_id).fadeOut(200);
				}
		});
		$('input[type="submit"]').click(function(event){
			event.preventDefault(event);
			var id=$(this).attr('id');
			var price_id="#price"+id;
			var qty_id="#qty"+id;
			var stock_id="#stock"+id;
			var error_id="#error"+id;
			var prev_qty_id="#prev_qty"+id;
			var price=$(price_id).val();
			var stock=$(stock_id).val();
			var qty=$(qty_id).val();
			$.post('process.php',{"sell_stock":stock,"price":price,"qty":qty},function(data){
				//alert(data);
				if($.trim(data)=="pass")
				{
					$(error_id).empty().html("<span class='text-success'>Success</span>");
					var email=$('#email').val();
					$.post('process.php',{"get_stock_qty":stock},function(data2){
						//alert(data2);
						$(prev_qty_id).empty().text($.trim(data2));
					});
				}
				else if ($.trim(data)=="fail")
				{
					$(error_id).empty().html("<span class='text-danger'>Error!</span>");
				}
				else
				{
					$(error_id).empty().html("<span class='text-warning'>Check Internet Connection</span>");
				}
			});
		});
	});
</script>
