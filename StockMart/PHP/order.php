<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$event=event2;
$content=$db->get_sm_place_order_table();
$js=<<<_END
	<script type="text/javascript">
		$(document).ready(function(){
			$('input[type="checkbox"]').click(function(){
				var cls=$(this).attr('class');
				if($(this).prop('checked')==true)
				{
					$('tr.'+cls).fadeIn(300);
				}
				if($(this).prop('checked')==false)
				{
					$('tr.'+cls).fadeOut(300);
					var cls2=".error"+cls;
					$(cls2).hide().empty();
				}
			});
			$('form').submit(function(event){
				event.preventDefault();
				var flag=true;
				var cls=$(this).attr('class');
				var price="#price"+cls;
				var qty="#qty"+cls
				var price_val=$(price).val();
				var qty_val=$(qty).val();
				if(isNaN(price_val) || price_val<=0 ||$.trim(price_val)=="")
				{
					var id="#div-price"+cls;
					$(id).removeClass('has-success');
					$(id).addClass('has-error');
					flag=false;
					var cls2=".error"+cls;
					$(cls2).hide().empty();   
				}
				else
				{
					var id="#div-price"+cls;
					$(id).removeClass('has-error');
					$(id).addClass('has-success');
					var cls2=".error"+cls;
					$(cls2).hide().empty();
				}
				if(isNaN(qty_val) || qty_val<=0 ||$.trim(qty_val)=="")
				{
					var id="#div-qty"+cls;
					$(id).removeClass('has-success');
					$(id).addClass('has-error');
					flag=false;
					var cls2=".error"+cls;
					$(cls2).hide().empty();
				}
				else
				{
					var id="#div-qty"+cls;
					$(id).removeClass('has-error');
					$(id).addClass('has-success');
					var cls2=".error"+cls;
					$(cls2).hide().empty();
				}
				if(flag)
				{
					var send=$(this).serialize();
					$.post('process.php',send,function(data){
						var id1="#div-price"+cls;
						var id2="#div-qty"+cls;
						var id3="#price"+cls;
						var id4="#qty"+cls;
						//alert(data);
						if($.trim(data)=="pass")
						{
							$(id1).removeClass('has-error');
							$(id1).addClass('has-success');
							$(id2).removeClass('has-error');
							$(id2).addClass('has-success');
							$(id3).val("");
							$(id4).val("");
							var cls2=".error"+cls;
							$(cls2).hide().empty().html("<span class='text-success'>Order Placed</span>").fadeIn('fast');
							$.post('process.php',{'get_balance':'email'},function(data2){
								$('#my_balance').empty().text("Balance : Rs. "+$.trim(data2));
							});
						}
						else if($.trim(data)=="fail")
						{
							$(id1).removeClass('has-success');
							$(id1).addClass('has-error');
							$(id2).removeClass('has-success');
							$(id2).addClass('has-error');
							var cls2=".error"+cls;
							$(cls2).hide().empty().html("<span class='text-danger'>Low balance !!! Sorry, can't place this order:-(</span>").fadeIn('fast');
						}
						else
						{
							$(id1).removeClass('has-success');
							$(id1).addClass('has-error');
							$(id2).removeClass('has-success');
							$(id2).addClass('has-error');
							var cls2=".error"+cls;
							$(cls2).hide().empty().html("<span class='text-danger'>Please, check your internet connection or login again.</span>").fadeIn('fast');
						}
					});
				}
			});
		});
	</script>
_END;
echo $db->echo_sm_template($email,$content.$js); 
?>

