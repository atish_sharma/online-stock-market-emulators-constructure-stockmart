<?php
require_once('../../Framework/framework.php');
$db=new db();
$event=event2;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../JavaScript Plugins/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">  
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
    <script src="../../JavaScript Plugins/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script src="../../JavaScript Plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>	
    <script src="../JS/validate.js"></script>
    <title>title</title>
  </head>
  <body style="padding-top:70px;">	
    
    <!-- navbar starts from here -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    	<div class="container">  
    		<div class="navbar-brand"><?php echo $event;?></div>
    		<ul class="nav navbar-nav navbar-right">
    			<li><a href="#" style="font-weight:bold;color:#428bca;" data-toggle="modal" data-target="#myModal">Sign Up</a></li>
    		</ul>
    	</div>
    </div>
    <!-- navbar ends here -->
    
    <!-- jumbotron starts from here -->
    <div class="jumbotron col-md-4 col-md-offset-4">
    	<strong style="font-weight:bold;color:#428bca;" class="col-md-4 col-md-offset-4">Sign In</strong>
    	<hr/>
    	<form action="signin.php" method="POST" class="form-horizontal" role="form" id="signin">
		 <div class="form-group">
		 	<div id="signin_error"></div>
			<input class="form-control" type="text" placeholder="Email" name="email"/>
			<input class="form-control" type="password" placeholder="Password" name="password"/>
			<br/>
			<button type="submit" class="btn btn-primary col-md-2 col-md-offset-10">Sign In</button>
		</div>
    	</form>
    </div>
    <!-- jumbotron ends here -->
    
    	<!-- Modal starts -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title text-primary" id="myModalLabel">Sign Up</h4>
	      </div>
	      <div class="modal-body">
		<form action="signup.php" method="POST" class="form-horizontal" role="form" id="signup">
			 <div class="form-group">
				<input class="form-control" type="text" placeholder="Name" name="name"/>
				<div id="signup_error"></div>
				<input class="form-control" type="text" placeholder="Email" name="email"/>
				<input class="form-control" type="password" placeholder="Password" name="password1" id="password"/>
				<input class="form-control" type="password" placeholder="Verify Password" name="password2"/>
			</div>
	      </div>
	      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Sign Up</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
	<!-- Modal ends -->
	
  </body>
</html>
