<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$event=event2;
$content=$db->get_sm_remove_sale_table($email);
echo $db->echo_sm_template($email,$content); 
?>
<script type='text/javascript'>
	$(document).ready(function(){
		$('#select_all').click(function(){
			$('input[type="checkbox"]').prop('checked',true);
		});
		$('#deselect_all').click(function(){
			$('input[type="checkbox"]').prop('checked',false);
		});
	});
</script>
