<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$event=event2;
$name=$db->get_name($email);
$balance=db::fix_currency($db->get_sm_balance($email));
$profit=$db->get_sm_net_profit($email);
$cls="text-primary";
if($profi<0)$cls="text-danger";
else if($profit>0)$cls="text-success";
$content=<<<_END
	<div class='well'>
		<table class='table table-bordered'>
			<h3>
				Portfolio
			</h3>
			<tr>
				<td align='right'>
					Name
				</td>
				<td align='left' style='font-weight:bold'>
					$name
				</td>
			</tr>
			<tr>
				<td align='right'>
					Email
				</td>
				<td align='left' style='font-weight:bold'>
					$email
				</td>
			</tr>
			<tr>
				<td align='right'>
					Balance
				</td>
				<td align='left' style='font-weight:bold'>
					Rs. $balance
				</td>
			</tr>
			<tr>
				<td align='right'>
					Net Profit
				</td>
				<td align='left' style='font-weight:bold'>
					<span class='$cls'>$profit %</span>
				</td>
			</tr>
		</table>
	</div>
_END;
echo $db->echo_sm_template($email,$content); 
?>

