<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$event=event2;
$count=0;
if(isset($_POST['num_remove_order']))
{
	$num=$_POST['num_remove_order'];
	for($i=1;$i<=$num;$i++)
	{
		if(isset($_POST[$i]))
		{
			$id=$_POST[$i];
			$db->deactivate_from_table_sm_orders($id);
			$count++;
		}
	}
}
$order="orders";
if($count==1)$order="order";
$content=<<<_END
	<span class="col-md-4 well text-success col-md-offset-4">
	$count $order removed.
	<a href="active_order.php" class="btn btn-success"> Go Back</a>
	</span>
_END;
echo $db->echo_sm_template($email,$content); 
?>

