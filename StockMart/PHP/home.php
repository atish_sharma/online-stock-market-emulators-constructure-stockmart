<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$db->create_sm_player($email);
$event=event2;
$table=$db->get_sm_top_gainers_table();
$content=<<<_END
		<div class="row">
    			<div class="well">
    				<b>Welcome to $event</b>
    				<br/>
    				<i>Here are some things that you should know about $event:</i>
    				<br/>
    				<ul>
    				<li><b class='text-primary'>Net Profit</b> is dependent on the price at which you buy and sell a stock.</li>
    				<li><b class='text-primary'>Average Buying Price</b> of a stock is the average of the price at which you and only you have purchased that particular stock.</li>
    				<li><b class='text-primary'>Market Price</b> of a stock is completely dependent on the price at which it is being sold in the market.</li>
    				</ul>
    			</div>
    			$table
    		</div>
_END;
echo $db->echo_sm_template($email,$content); 
?>

