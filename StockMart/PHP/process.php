<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['stockmart_email']))
{
	db::logout();
}
$email=$_SESSION['stockmart_email'];
$event=event2;
if(isset($_POST['order_stock']))
{
	$stock=$_POST['order_stock'];
	$price=$_POST['price'];
	$qty=$_POST['qty'];
	if($db->sm_place_order($email,$stock,$qty,$price))echo "pass";
	else echo "fail";
}
else if(isset($_POST['sell_stock']))
{
	$stock=$_POST['sell_stock'];
	$price=$_POST['price'];
	$qty=$_POST['qty'];
	if($db->put_sm_stocks_on_sale($stock,$email,$qty,$price))echo "pass";
	else echo "fail";
}
else if (isset($_POST['get_stock_qty']))
{
	$stock=$_POST['get_stock_qty'];
	echo $db->get_sm_stock_inventory_qty($email,$stock);
}
else if(isset($_POST['get_balance']))
{
	echo $db->get_sm_currency($email);		
}
?>

