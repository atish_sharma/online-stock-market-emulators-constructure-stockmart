<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
$balance=$db->get_constructure_balance($email);
$event=event1;
$event=db::fix_name($event);
$item="";
?>
<div id="balance">
	<i class="white">My Balance</i>
	<br/>
	<span class="white">Rs.</span><b style="color:red;" id='my-balance'><?php echo $balance; ?></b>
</div>
<div id="slider">
	<div>
		<h3 style="color:red;font-family:impact;font-weight:bold;text-align:center;">Welcome to <?php echo $event; ?></h3>
		<font color="white">
		<ul>	
			<b>Rules:</b>
			<li>Each participant will receive a particular budget at the start of the event.</li>
			<li>Each item for construction will have a base value.</li>
			<li>Item price will rise as more stocks of the material are bought.</li>
			<li>You can buy stocks (items) from the market as well as other stockholders (shareholders).</li>
			<li>You can put your stocks on sale at a fixed price.</li>
			<li>Your aim is to buy the required number of stocks in order to construct the given structure and click the <i>Submit Structure</i><sub> [situated at the top]</sub> button before the end of the event.</li>
			<li>Incase you are unable to understand anything, try looking for an answer in the F.A.Q. section [Go to Dashboard->F.A.Q.].</li>
		</ul>
		</font>
			<b style="text-align:center;color:red;"><<< Start from the dashboard <<< </b>
	</div>
	<div>
		<img src="../../MEDIA/Pics/9-Blue-Tokyo-Japan.jpg" alt="Buy" title="Buy" width="500" height="700"/>
	</div>
	<div>
		<img src="../../MEDIA/Pics/29.jpg" alt="Sell" title="Sell" width="500" height="700"/>
	</div>
	<div>
		<img src="../../MEDIA/Pics/Mumbai_Skyline_at_Night.jpg" alt="Build" title="Build" width="500" height="700"/>
	</div>
</div>
<div id="faq">
	<br/>
	<table class="black-font" style='border-collapse:collapse;' cellpadding='9px' width='75%'>
		<tr style='background-color:white'>
			<td colspan='2'>
				F.A.Q. <sub>[Frequently Asked Questions]</sub>
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q1.</b>
			</td>
			<td align="left">
				What is Constructure ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A1.</b>
			</td>
			<td align="left">
				Constructure is an interactive stock selling & buying online event where you have to construct a given structure by buying the required quantity of items.
				You can buy stocks from the market and other stockholders.You can also put your stocks for sale at a fixed price.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q2.</b>
			</td>
			<td align="left">
				How do I get to know my structure requirements ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A2.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> My Stocks -> My Structure.
				<br/>
				<i>Required Quantity</i> is the minimum quantity required to complete your structure.
				<br/>
				<i>Quantity in Inventory</i> is the qunatity you own.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q3.</b>
			</td>
			<td align="left">
				How do I buy from the market ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A3.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> Visit Market.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q4.</b>
			</td>
			<td align="left">
				How do I buy from other shareholders ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A4.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> Other Shareholders -> Buy Stocks from Other Shareholders.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q5.</b>
			</td>
			<td align="left">
				How do I put my stocks on sale ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A5.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> Other Shareholders -> Put my Stocks on Sale.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q6.</b>
			</td>
			<td align="left">
				I just put some stocks on sale, how do I get them back ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A6.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> Other Shareholders -> My Stocks on Sale.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q7.</b>
			</td>
			<td align="left">
				Where do I view my Stocks ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A7.</b>
			</td>
			<td align="left">
				Go to the Dashboard [located at the left of the screen] -> My Stocks -> My Inventory.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q8.</b>
			</td>
			<td align="left">
				Can I view my transactions ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A8.</b>
			</td>
			<td align="left">
				Yes, you can view your transactions.
				<br/>
				Go to the Dashboard [located at the left of the screen] -> My Stocks -> My Transactions.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q9.</b>
			</td>
			<td align="left">
				After putting some of my stocks on sale I can't view them in My Inventory ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A9.</b>
			</td>
			<td align="left">
				That is because you put them on sale and they will not be a part of your inventory till you restore them.
			</td>
		</tr>
		<tr style='background-color:yellow'>
			<td align="left" valign="top">
				<b>Q10.</b>
			</td>
			<td align="left">
				Where can I view my current balance ?
			</td>
		</tr>
		<tr style='background-color:orange'>
			<td align="left" valign="top">
				<b>A10.</b>
			</td>
			<td align="left">
				You can see your balance somwhere below the top-right side of the screen.
				<br/>
				If your balance is hidden, go to the Dashboard [located at the left of the screen] -> Show Balance.
			</td>
		</tr>
	</table>
</div>
<div id="leaderboard-table">
	<?php echo $db->get_constructure_winners_main(); ?>
</div>