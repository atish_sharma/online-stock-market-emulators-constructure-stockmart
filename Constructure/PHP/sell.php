<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
$event=event1;
$table=$db->get_sell_table($email);
$footer="<div id='footer-right'>&copy; TechTatva 2014&nbsp;</div>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body2.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.chk').each(function(){
			var id=$(this).attr('id');
			var table='#sell-this'+id;
			if($(this).val()=="Put this Item on Sale")
			{
				$(table).hide();
			}
			else
			{	
				$(table).show();	
			}
		});
		$('.chk').bind('click',function(){
			var id=$(this).attr('id');
			var table='#sell-this'+id;
			if($(this).val()=="Put this Item on Sale")
			{
				$('.a_table').hide();
				$('.chk').val('Put this Item on Sale');
				$(table).fadeIn(200);
				$(this).val("Do Not Put this Item on Sale");
			}
			else
			{
				$(table).fadeOut(200);	
				$(this).val('Put this Item on Sale');
			}	
		});
		$('.sell').click(function(event){
			var flag=true;
			var id=$(this).attr('name');
			$('.price'+id).each(function(){
				var val=$(this).val();
				if(Number(val))
				{
					if(val<=0) 
					{
						$(this).css({"background-color":"red"});
						event.preventDefault();
						flag=false;
					}
					else 
					{
						$(this).css({"background-color":"white"})
					};
				}
				else 
				{
					$(this).css({"background-color":"red"});
					event.preventDefault();
					flag=false;
				}
			});
			$('.qty'+id).each(function(){
				var max=$(this).attr('max');
				var val=$(this).val();
				max=parseInt(max)
				if(Number(val))
				{
					if(val>max || val<1) 
					{
						$(this).css({"background-color":"red"});
						event.preventDefault();
						flag=false;
					}
					else
					{
						$(this).css({"background-color":"white"})
					};
				}
				else 
				{
					$(this).css({"background-color":"red"});
					event.preventDefault();
					flag=false;
				}
			});
		});
	});
	</script>
	<title><?php echo $event; ?></title>
</head>
<body>
	<div id="top-left-header"><?php echo $event; ?></div>
	<div id="top-right-header"><span id="name"><?php echo "Balance : Rs. ".$db->get_constructure_balance($email)." | ".$db->get_name($email); ?></div>
	<?php echo $table.$footer; ?>
</body>
</html>
