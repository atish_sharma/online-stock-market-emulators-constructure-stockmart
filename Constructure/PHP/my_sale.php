<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
$event=event1;
$table=$db->get_table_restore_sale_item($email);
$footer="<div id='footer-right'>&copy; TechTatva 2014&nbsp;</div>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body2.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
			$('#all').click(function(){
				$(':checkbox').prop('checked',true);
			});
			$('#none').click(function(){
				$(':checkbox').prop('checked',false);
			});
	});
	</script>
	<title><?php echo $event; ?></title>
</head>
<body>
	<div id="top-left-header"><?php echo $event; ?></div>
	<div id="top-right-header"><span id="name"><?php echo "Balance : Rs. ".$db->get_constructure_balance($email)." | ".$db->get_name($email); ?></div>
	<?php echo $table.$footer; ?>
</body>
</html>
