<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['constructure_email']))
{
	db::logout();
}
$email=$_SESSION['constructure_email'];
$name=$db->get_name($email);
$db->create_new_constructure_player($email);
if($db->check_structure($email))
{
	header("Location:structure_complete.php");
}
$balance=$db->get_constructure_balance($email);
$raw_balance=$db->get_raw_constructure_balance($email);
$item_table=$db->get_constructure_item_table();
$winner="";
if($db->get_constructure_winners())
{
	$winner='<div class="dashboard-selection animated-selection white hand-cursor" id="leaderboard-tab">Leaderboard</div><hr/>';
}
$event=event1;
echo <<<_END
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<link rel="stylesheet" href="../../JavaScript Plugins/CSS-Tricks-AnythingSlider-b72e317/css/anythingslider.css"/>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
	<script src="../../JavaScript Plugins/CSS-Tricks-AnythingSlider-b72e317/js/jquery.anythingslider.min.js"></script>
	<script src="../JS/home.js"></script>
	<title>$event</title>
</head>
<body>
	<div id="top-left-header">$event</div>
	<div id="top-right-header"><span class='animated-selection hand-cursor' id='structure'>Submit Structure</span> | <span id="name">$name</span> | <span id="logout" class="animated-selection hand-cursor">Logout</span>&nbsp;</div>
	<div id="main"></div>
	<table id="dashboard-panel" class="hand-cursor">
		<tr>
			<td class="white">D</td>
		</tr>
		<tr>
			<td class="white">A</td>
		</tr>
		<tr>
			<td class="white">S</td>
		</tr>
		<tr>
			<td class="white">H</td>
		</tr>
		<tr>
			<td class="white">B</td>
		</tr>
		<tr>
			<td class="white">O</td>
		</tr>
		<tr>
			<td class="white">A</td>
		</tr>
		<tr>
			<td class="white">R</td>
		</tr>
		<tr>
			<td class="white">D</td>
		</tr>
	</table>
	<div id="dashboard">
		<span id="dashboard-selections">
		<div class="dashboard-selection animated-selection white hand-cursor" id="home">Home</div>
		<hr/>
		<div class="dashboard-selection animated-selection white hand-cursor" id="buy">Visit Market</div>
		<hr/>
		<div class="dashboard-selection animated-selection white hand-cursor" id="others">Other Shareholders</div>
		<hr/>
		<div class="dashboard-selection animated-selection white hand-cursor" id="my">My Stocks</div>
		<hr/>
		<div class="dashboard-selection animated-selection white hand-cursor" id="show_balance">Hide Balance</div>
		<hr/>
		<div class="dashboard-selection animated-selection white hand-cursor" id="refresh">Refresh Balance</div>
		<hr/>
		$winner
		<div class="dashboard-selection animated-selection white hand-cursor" id="faq">F.A.Q.</div>
		</span>
	</div>
	<div id="balance-widget">
	</div>
	<div id="footer-right">
		&copy; TechTatva 2014&nbsp;
	</div>
	
	<!-- This is for hidden divs only -->
	
	$item_table
	<div id="read_me" style="display:none;">
	<div class="red-font">Read Me !!!</div>
	<div id="read_me_content"></div>
	</div>
	<div id="constructure_email" style="display:none;">$email</div>
	<div id="raw-balance" style="display:none;">$raw_balance</div>
	<div id="imp" style="display:none;">
	<div id="imp-heading"><b><font color='red'>Important Stuff</font></b></div>
	<hr width='100%'/>
	<div id="imp-content"></div>
	<div id="imp-button-div">
	<input type="button" id="imp-button" value="OK" class='hand-cursor not-disable' style="text-align:center;width:100%;background-color:red;border-radius:3px;border:3px solid red;font-weight:bold;color:white;"/>
	<input type="button" id="imp-button2" value="Cancel" class='hand-cursor not-disable' style="text-align:center;width:100%;background-color:red;border-radius:3px;border:3px solid red;font-weight:bold;color:white;display:none"/>
	<input type="button" id="imp-button3" value="Continue" class='hand-cursor not-disable' style="text-align:center;width:100%;background-color:green;border-radius:3px;border:3px solid green;font-weight:bold;color:white;display:none"/>
	</div>
	</div>
	<div id="my_stuff" class='center' style="display:none;">
		<span id="my_inventory" class="animated-selection white hand-cursor">My Inventory</span><span class='white'> | </span>
		<span id="my_transaction" class="animated-selection white hand-cursor">My Transactions</span><span class='white'> | </span>
		<span id="my_structure" class="animated-selection white hand-cursor">My Structure</span> 
		<hr width='100%'>
		<div id="my_content"></div>
	</div>
	<div id="shares" class='center' style="display:none;">
		<blockquote style="text-align:center;color:red;font-family:impact;font-weight:bold;font-size:30px;width:65%;">
		"Buying from the market is an easy job ... this is where everything gets tough !!! 
		This is where you have to sell your stocks at a higher price than you bought and buy from others at a lower price than the market price.
		Of course, you don't need to do this unless you want to survive in this environment."
		<br/>
		<a href="sell.php" target="_blank" class="animated-selection white hand-cursor" style='text-decoration:none;'>Put my Stocks on Sale</a><span class='white'> | </span>
		<a href="others.php" target="_blank" class="animated-selection white hand-cursor" style='text-decoration:none;'>Buy Stocks from Other Shareholders</a><span class='white'> | </span>
		<a href="my_sale.php" target="_blank" class="animated-selection white hand-cursor" style='text-decoration:none;'>My Stocks on Sale</a> 
		</blockquote>
	</div>
	<div id="leaderboard" class='center' style="display:none;">
	</div>
	
	<!-- The hidden divs ends here -->
</body>
</html>
_END;
?>
