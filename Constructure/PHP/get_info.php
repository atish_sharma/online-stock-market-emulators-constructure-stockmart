<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
$item="";
if(isset($_POST['market_price_item']))
{
	$item=$db->get_constructure_item_value($_POST['market_price_item']);
	echo db::fix_currency($item[0]); 	
}
else if(isset($_POST['market_qty_item']))
{
	$item=$db->get_constructure_item_value($_POST['market_qty_item']);
	echo $item[1]; 	
}
else if (isset($_POST['market_update_item']))
{
	$item=db::get_date();
	echo $item; 
}
else if(isset($_POST['raw_balance']))
{
	$item=$db->get_raw_constructure_balance($email);
	echo $item;
}
else if(isset($_POST['item_name']) && isset($_POST['item_qty'])) echo $db->buy($email,$_POST['item_name'],$_POST['item_qty']);
?>

