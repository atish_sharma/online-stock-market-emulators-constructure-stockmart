<?php
require_once('../../Framework/framework.php');
$db=new db();
$event=event1;
echo <<<_END
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#signup').hide();
			$('#footer-left').click(function(){
				var val=$(this).val();
				if(val=="sign in")
				{
					$('#signup').hide();
					$('#signin').slideDown('slow');
					$(this).val('sign up');
				}
				else
				{
					$('#signin').hide();
					$('#signup').slideDown('slow');
					$(this).val('sign in');
				}
			});
			$('#signin').validate({
					rules:{
						email:{
							required:true,
							email:true
						},
						password:{
							required:true
						}
					},
					messages:{
						email:{
							required:"<br/>Please enter your email",
							email:"<br/>Please enter a valid email"
						},
						password:{
							required:"<br/>Please enter your password"
						}
					}
				});
			$('#signin').submit(function(event){
				event.preventDefault();
				var flag=true;
				$('#signin input.error').each(function(){
					if($(this).attr('class')=="error")
					{
						flag=false;
					}
				});
				if(flag)
				{
					var query=$('#signin').serialize();
					$.post('signin.php',query,function(data){
						data=$.trim(data);
						if(data=="fail")
						{
							$('#signin_error').text("Wrong email/password combination");
						}
						else if(data=="pass")
						{
							$('#signin_error').text("");
							window.location="home.php";
						}
					});
				}
			});
			$('#signup').validate({
				rules:{
						name:{
							required:true
						},
						email:{
							required:true,
							email:true
						},
						password1:{
							required:true,
							rangelength:[8,16]
						},
						password2:{
							equalTo:'#password'
						}
					},
					messages:{
						name:{
							required:"<br/>Please enter your name"
						},
						email:{
							required:"<br/>Please enter your email",
							email:"<br/>Please enter a valid email"
						},
						password1:{
							required:"<br/>Please enter a password",
							rangelength:"<br/>Password should be 8 to 16 characters long"
						},
						password2:{
							equalTo:"<br/>Passwords do not match"
						}
					}
				});
			$('#signup').submit(function(event){
				event.preventDefault();
				var flag=true;
				$('#signup input.error').each(function(){
					if($(this).attr('class')=="error")
					{
						flag=false;
					}
				});
				if(flag)
				{
					var query=$('#signup').serialize();
					$.post('signup.php',query,function(data){
						data=$.trim(data);
						if(data=="fail")
						{
							$('#signup_error').text("This email is already in use :-(");
						}
						else if(data=="pass")
						{
							$('#signup_error').text("");
							window.location="home.php";
						}
						});
				}
			});
		});
	</script>
	<title>
		$event | Log In
	</title>
</head>
<body>
	<div id="top-bar">
		<h1>$event</h1>
	</div>
	<form id="signin" action="#" method="POST">	
		<table align="center">
			<tr align="center">
				<td>
					<div id="signin_error"></div>
					<input type="text" name="email" placeholder="Email"/>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="password" name="password" placeholder="Password"/>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="submit" value="sign in" id="signin-btn" class="btn hand-cursor"/>
				</td>
			</tr>
		</table>
	</form>
	<form id="signup" name="signup_form" action="home.php" method="POST" style='display:none;'>	
		<table align="center">
			<tr align="center">
				<td>
					<input type="text" name="name" placeholder="Name"/>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="text" name="email" placeholder="Email"/>
					<div id="signup_error"></div>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="password" name="password1" placeholder="Password" id="password"/>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="password" name="password2" placeholder="Verify Password"/>
				</td>
			</tr>
			<tr align="center">
				<td>
					<input type="submit" value="sign up" id="signup-btn" class="btn hand-cursor"/>
				</td>
			</tr>
		</table>
	</form>
	<input type="button" id="footer-left" value="sign up" class="btn hand-cursor"/>
	<div id="footer-right">
		&copy; TechTatva 2014&nbsp;
	</div>
</body>
</html>
_END;
?>
