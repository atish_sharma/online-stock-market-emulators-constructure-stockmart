<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
if(isset($_POST['my_inventory']))
{
	echo $db->get_constructure_inventory($email);
}
else if(isset($_POST['my_transaction']))
{
	echo $db->get_constructure_transaction($email);
}
else if(isset($_POST['my_structure']))
{
	echo $db->get_constructure_structure($email);
}
else if(isset($_POST['chk_structure']))
{
	if($db->check_structure($email))
	{
		echo "pass";
	}
	else echo "fail";
}
?>

