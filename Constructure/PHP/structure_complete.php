<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(!isset($_SESSION['constructure_email']))
{
	db::logout();
}
$email=$_SESSION['constructure_email'];
$name=$db->get_name($email);
$date=$db->get_completion_date($email);
$event=event1;
$balance=$db->get_completion_balance($email);
$table=$db->get_constructure_winners();
$table="<table style='border:10px solid white;background-color:green;margin-top:120px;padding:21px;border-radius:12px;' width='65%' align='center'>
		<tr>
			<td>
				*Congratulations $name !!!
				<br/>
				You have completed the structure on $date ,
				<br/>
				with a surplus balance of Rs. $balance :-)
				<br/>
				<i>Don't worry if you haven't clicked the 'Submit Structure' button ... your structure is submitted the moment the server realizes your structure is complete !</i>
			</td>
		</tr>

	</table><br/><hr width='65%'/><br/>$table";
$footer="<div style='position:absolute;left:0px;bottom:0px;width:15%;'><sub>*You cannot participate in Constructure after structure completion.</sub></div><div id='footer-right'>&copy; TechTatva 2014&nbsp;</div>";
?>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body2.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<title><?php echo $event; ?></title>
</head>
<body>
	<div id="top-left-header"><?php echo $event; ?></div>
	<div id="top-right-header"><span id="name"><?php echo "Balance : Rs. ".$db->get_completion_balance($email)." | ".$db->get_name($email)." | "; ?><a href='logout.php' style="text-decoration:none;" class="white hand-cursor">Logout</a>&nbsp;</div>
	<?php echo $table.$footer; ?>
</body>
</html>
