<?php
require_once("../../Framework/framework.php");
$db=new db();
session_start();
if(isset($_SESSION['constructure_email']))
$email=$_SESSION['constructure_email'];
else db::log_out();
$event=event1;
$quantity=0;
$cost=0;
$item="";
if(isset($_POST['item']))
{
	$item=$_POST['item'];
	$qty=$_POST['qty'];
	$price=$_POST['price'];
	$reply=$db->sell_others_stock($email,$price,$qty,$item);
	$quantity=$reply[0];
	$cost=$reply[1];
}
$table="<table><tr><td>";
if($quantity>0)
{
	$table.="You just bought $quantity $item(s) worth a total cost of Rs. $cost";	
}
else
{
	$table.="Request Unsuccessfull !!! Please try again or try at higher prices. Also check that the product of your entered quantity and unit price is not greater than your balance.";	
}
$table.="</td></tr><tr><td align='center'><input type='button' value='Go Back'/></td></tr></table>";
$footer="<div id='footer-right'>&copy; TechTatva 2014&nbsp;</div>";
?>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" href="../CSS/body2.css"/>
	<link rel="stylesheet" href="../CSS/placement.css"/>
	<link rel="stylesheet" href="../CSS/btn.css"/>
	<script src="../../JavaScript Plugins/jquery-validation-1.11.1/lib/jquery-1.9.0.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(':input').click(function(){window.location="others.php";})
	});
	</script>
	<title><?php echo $event; ?></title>
</head>
<body>
	<div id="top-left-header"><?php echo $event; ?></div>
	<div id="top-right-header"><span id="name"><?php echo "Balance : Rs. ".$db->get_constructure_balance($email)." | ".$db->get_name($email); ?></div>
	<div class='center white'>
	<?php echo $table; ?>	
	</div>
	<?php echo $footer; ?>
</body>
</html>
