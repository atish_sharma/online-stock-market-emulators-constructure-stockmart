$(document).ready(function(){
		load_home();
		$('#home').click(load_home);
		$('#faq').click(load_faq);
		$('#balance-widget').hide().load('main.php #balance').fadeIn('slow');
		function load_home()
		{
			hide_stuff();
			$('#main').empty();
			$('#main').hide();
			show_read_me("Loading...");
			$('#main').load('main.php #slider',function(){
				$('#slider').anythingSlider({
					autoPlay:false
				});
			});
			$('#main').fadeIn('slow');
			hide_read_me();
		}
		function load_faq()
		{
			hide_stuff();
			$('#main').empty();
			$('#main').hide();
			show_read_me("Loading...");
			$('#main').load('main.php #faq');
			$('#main').fadeIn('slow');
			hide_read_me();
		}
		$('#logout').click(function(){
			window.location="logout.php";
		});
		$('.animated-selection').hover(function(){
				$(this).css({
				'background-color':'white',
				'color':'#00255f',
				'border-radius':'3px',
				});
				$(this).stop().animate({
						fontSize:'20px',
						padding:'3px'
				},
				400,
				'swing');
		},
		function(){
			$(this).css({
				'background-color':'#00255f',
				'color':'white',
				'border-radius':'0px'
				});
			$(this).stop().animate({
						fontSize:'16px',
						padding:'0px'
				},
				400,
				'swing');
		});
		$('#dashboard-panel').hover(function(){
				//load_balance();
				$('#dashboard').stop().animate({
						left:'0px'
				},
				400,
				'swing');
				$(this).fadeOut(400);
		},
		function(){
			$('#dashboard').hover(function(){
				//load_balance();
				$(this).stop().animate({
					left:'0px'
				},
				400,
				'swing');
			},function(){
				$(this).stop().animate({
					left:'-150px'
				},
				400,
				'swing');
				$('#dashboard-panel').slideDown(400);
			});
		});
		$('#show_balance').click(function(){
				if($(this).text()=="Show Balance")
				{
					load_balance();
					$('#balance-widget').fadeIn(400);
					$(this).text("Hide Balance");
				}
				else
				{
					$('#balance-widget').fadeOut(400);
					$(this).text("Show Balance");
				}	
		});
		function load_balance()
		{
			$('#balance-widget').empty();
			show_read_me("Refreshing your balance ...");
			$('#balance-widget').load('main.php #balance');
			hide_read_me();
		}
		$('input[type="button"]').click(function(){
				//load_balance();load_raw_balance();update_market_table();
		});
		$('#balance-widget').hover(function(){
			show_read_me("This is your current balance. You can refresh it from the dashboard.");
		},function(){
			hide_read_me();
		});
		$('#buy').click(function(){
			hide_stuff();
			$('#buy_from_market').fadeIn('slow');
		});
		function hide_stuff()
		{
			$('#main').hide().empty();
			$('#buy_from_market').hide();
			$('#my_stuff').hide();
			$('#shares').hide();
			$('#leaderboard').hide();
		}
		function hide_read_me()
		{
			$('#read_me').slideUp(200);
		}
		function show_read_me(text)
		{
			hide_read_me();
			$('#read_me_content').empty().html(text);
			$('#read_me').slideDown(100);
		}
		function update_market_table()
		{
			$('#buy_from_market tr td input[type="hidden"]').each(function(){
				val=$(this).val();
				var cls=$(this).attr('class');
				$.post('get_info.php',{market_price_item:val},function(data){
					var c="#market_price_"+cls;	//c for class
					$(c).text("Rs. "+data);
				});
				$.post('get_info.php',{market_qty_item:val},function(data){
					var c="#market_qty_"+cls;	//c for class
					$(c).text(data);
				});
			});
			$.post('get_info.php',{market_update_item:val},function(data){
					$('#market_update_date').text(data);
				});
		}
		$('#buy_from_market').hover(function(){
			if(!check_qty())show_read_me('Stock price and quantity may change dramatically because it updates itself each time you hover over the item table.');
			update_market_table();
		},function(){hide_read_me();});
		function check_qty()
		{
			no_num=false;
			higher_num=false;
			$('#buy_from_market tr td input[type="checkbox"]').each(function(){
				var id=$(this).attr('class');
				var qty=$("#market_qty_"+id).text();
				id='input[type="text"].'+id;
				var val=$(id).val();
				var val_str=val;
				val=parseInt(val);
				qty=parseInt(qty);
				$(id).css({
					'background-color':'white'
				});
				if($(this).is(':checked'))
				{
					if( !(validate_num(val_str)) || $.trim(val)=="" || val<1 || val_str=="" || $.trim(val_str)=="" )
					{
						no_num=true;
						$(id).css({
							'background-color':'red'
						});
						$(id).focus();
					}
					else if(val>qty)
					{
						$(id).css({
							'background-color':'red'
						});
						$(id).focus();
						higher_num=true;
					}
				}	
			});
			if(no_num)
			{
				var error="Please enter a non-zero positive integer in the 'Purchase Quantity' column.";
				show_read_me(error);
				market_buy_error(error);
				imp(error);
				return true;
			}
			if(higher_num)
			{
				var error="You can't buy more stocks than are available.";
				show_read_me(error);
				market_buy_error(error);
				imp(error);
				return true;	
			}
			return false;
		}
		function market_buy_error(text)
		{
			$('#market_buy_error').hide().empty().html("OOPS : "+text).show();
		}
		function get_num(num)
		{
			var number="";
			for(var i=0;i<num.length;i++)
			{
				var flag=false;
				var n=num.charAt(i);
				if(Number(n)||n=="0") flag=true;
				else if(n=='.' && num.charAt(i-1)!='s')flag=true;
				if(flag) number+=n;
			}
			number=parseFloat(Number(number));
			number=number.toFixed(2);
			return number;
		}
		function validate_num(num)
		{
			for(var i=0;i<num.length;i++)
			{
				var n=num.charAt(i);
				if(isNaN(n)) return false;
			}
			return true;
		}
		function load_raw_balance()
		{
			$.post('get_info.php',{raw_balance:'raw'},function(data){
				$('#raw-balance').text(data);
			});
		}
		function get_raw_balance()
		{	
			load_raw_balance();
			var val=$('#raw-balance').text();
			$('#raw-balance').empty();
			val=get_num(val);
			return val;
		}
		function get_item_market_price(id)
		{
			id="#market_price_"+id;
			var val=$(id).html();
			val=get_num(val);
			return val;
		}
		function get_total_market_buy()
		{
			var total_price=0;
			var price=0;
			$('#buy_from_market tr td input[type="checkbox"]').each(function(){
				var $id=$(this).attr('class');
				id='input[type="text"].'+$id;
				var val=$(id).val();
				val=parseInt(val);
				id='input[type="hidden"].'+$id;
				var item=$(id).val();
				if($(this).is(':checked')){
					price=get_item_market_price($id);
					total_price+=val*price;
				}});
			total_price=total_price.toFixed(2);
			return total_price;
		}
		function imp(txt)
		{
			$('#imp').hide();
			$('#imp-content').empty().html(txt);
			$('#imp').css({
				'background-color':'white',
				'position':'absolute',
				'top':'210px',
				'right':'0px',
				'padding':'3px',
				'color':'#00255f',
				'width':'150px',
			}).fadeIn(200);
		}
		$('#imp-button').click(function(){
				$('#imp').fadeOut(200);
				$('#imp-content').empty();
				enable_input();
		});
		function disable_input()
		{
			$('input').not('.not-disable').attr('disabled',true);
		}
		function enable_input()
		{
			$('input').attr('disabled',false);
		}
		$('#buy_from_market tr td input[type="button"].market_buy').click(function(){
			disable_input();
			var enable=true;
			$('#market_buy_error').hide();
			hide_read_me();
			var flag=check_qty();
			if(!flag)
			{
				var flag=true;
				$('#buy_from_market tr td input[type="checkbox"]').each(function(){
				if($(this).is(':checked'))
				{
					flag=false;
				}
				});
				if(flag)
				{
					var error="Tick the buy checkbox and determine its purchase quantity to buy an item.";
					show_read_me(error);
					market_buy_error(error);
					imp(error);
				}
				else
				{
					var balance=Number(get_raw_balance());
					var total=Number(get_total_market_buy());
					var newBalance=balance-total;
					newBalance=newBalance.toFixed(2);
					if(balance<total)
					{
						var error="You only have <i>Rs. "+balance+"</i> but your purchase is worth <i>Rs. "+total+"</i><br><i><b>Transaction Cancelled</b></i>";
						show_read_me(error);
						market_buy_error(error);
						imp(error);
					}
					else
					{
						
						enable=false;
						$('#imp').hide();
						$('#imp-button').hide();
						$('#imp-button2').show();
						$('#imp-button3').show();
						imp("Total Cost: Rs.<b>"+total+"</b><br/>New Balance will be: Rs.<b>"+newBalance+"</b>");		
					}
				}
			}
			if(enable)enable_input();
		});
		$('#imp-button2').click(function(){
			$('#imp').hide();
			$('#imp-button2').hide();
			$('#imp-button3').hide();
			$('#imp-button').show();
			imp("<b>Transaction Cancelled</b><br/><sub>[Click <i>OK</i> to continue]</sub>");
		});
		$('#imp-button3').click(function(){
			$('#imp').hide();
			$('#imp-button2').hide();
			$('#imp-button3').hide();
			$('#imp-button').show();
			imp("<b>Transaction Confirmed</b>");
			show_read_me("<b style='color:black'>Processing Transaction</b>");
			imp("<b style='color:black'>Processing Transaction</b>");
			$('#buy_from_market tr td input[type="checkbox"]').each(function(){
			if($(this).is(':checked'))
			{
				var id=$(this).attr('class');
				var item=$('input[type="hidden"].'+id).val();
				var qty=$('input[type="text"].'+id).val();
				buy_item_from_market(item,qty);
			}
			});
			update_market_table();
			load_balance();
			load_raw_balance();
			imp("<b style='color:green;'>Transaction Complete</b><br/><sub>[Click <i>OK</i> to continue]</sub>");
			show_read_me("<b style='color:green'>Transaction Complete</b>");
		});
		$('#my').click(function(){
			hide_stuff();
			$('#my_stuff').fadeIn(400);
			get_my_inventory();
			$('#my_inventory').focus();
		});
		$('#my_inventory').click(function(){
			get_my_inventory();
			$(this).focus();
		});
		$('#my_transaction').click(function(){
			get_my_transaction();
			$(this).focus();
		});
		$('#my_structure').click(function(){
			get_my_structure();
			$(this).focus();
		});
		function get_my_inventory()
		{
			$.post('get_info2.php',{my_inventory:"my"},function(data){
				show_read_me("Loading...");
				$('#my_content').hide().empty().html(data).fadeIn(400);
				hide_read_me();
			});
		}
		function get_my_structure()
		{
			$.post('get_info2.php',{my_structure:"my"},function(data){
				show_read_me("Loading...");
				$('#my_content').hide().empty().html(data).fadeIn(400);
				hide_read_me();
			});
		}
		function get_my_transaction()
		{
			$.post('get_info2.php',{my_transaction:"my"},function(data){
				show_read_me("Loading...");
				$('#my_content').hide().empty().html(data).fadeIn(400);
				hide_read_me();
			});
		}
		function buy_item_from_market(item,qty)
		{
			$.post('get_info.php',{item_name:item,item_qty:qty},function(data){
			});
		}
		$('#others').click(function(){
				hide_stuff();
				$('#shares').fadeIn(400);
		});
		$('#structure').click(function(){
			window.location="is_complete.php";
		});
		$('#structure').hover(function(){
			var warning="If you click this button before completing your structure, your page will just get reloaded !";
			show_read_me(warning);	
		},
		function(){
			hide_read_me();
		});
		$("#home").hover(function(){
		show_read_me("Click here to visit home or if you want to read some rules.");
		},function(){
		hide_read_me();
		});
		$("#buy").hover(function(){
		show_read_me("Click here to visit the market and purchase some items.");
		},function(){
		hide_read_me();
		});
		$("#others").hover(function(){
		show_read_me("Click here to put your stocks on sale or buy from other stockholders.");
		},function(){
		hide_read_me();
		});
		$("#my").hover(function(){
		show_read_me("Click here to view your inventory, transactions or structure requirements.");
		},function(){
		hide_read_me();
		});
		$("#show_balance").hover(function(){
		show_read_me("Click here to hide/show your balance.");
		},function(){
		hide_read_me();
		});
		$("#faq").hover(function(){
		show_read_me("Can't understand something, visit the F.A.Q. section.");
		},function(){
		hide_read_me();
		});
		$("#refresh").hover(function(){
		show_read_me("Click it to reload your balance.");
		},function(){
		hide_read_me();
		});
		$("#leaderboard-tab").hover(function(){
		show_read_me("Click it to view Leaderboard.");
		},function(){
		hide_read_me();
		});
		$('#leaderboard-tab').click(function(){
			show_read_me("Loading...");
			hide_stuff();
			load_leaderboard();
			hide_read_me();
		});
		$('#refresh').click(function(){
			load_balance();
			$('#balance-widget').fadeIn(400);
			$('#show_balance').text("Hide Balance");
		});
		function load_leaderboard()
		{
			$('#leaderboard').hide().empty();
			$('#leaderboard').load('main.php #leaderboard-table');
			$('#leaderboard').fadeIn(400);
		}
});
